<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
 
<link href="<?=$this->config->item('theme_folder_url')?>third_party/<?=$module_name?>/css/<?=$module_name?>.css" rel="stylesheet" type="text/css" media="screen" />

<script type="text/javascript">
	
	jQuery.<?=$module_name?>CP = {
		currentSection: function() {
			if (window.location.hash && window.location.hash != '#') {
				return window.location.hash.substring(1);
			} else {
				return $('#<?=$module_name?>_settings_content h3:first').attr('data-hash');
			}
		},
		channels: <?=json_encode($channel_titles)?>,
		fields: <?=json_encode($fields)?>,
		checkSelectedChannel: function (selector, section) {
			if ($(selector).val() !="") {
				$(section).css("display","inline");
			} else {
				$(section).css("display","none");
			}
		},
		updateSelect: function(select, options) {
			var val = $(select).val();
			var attrs = {};
			for (i=0;i<select.attributes.length;i++) {
				if (select.attributes[i].name == 'value') {
					val = select.attributes[i].value;
				} else {
					attrs[select.attributes[i].name] = select.attributes[i].value;
				}
			}
			$(select).replaceWith($.<?=$module_name?>CP.createSelect(attrs, options, val));
		},
		createSelect: function(attributes, options, selected) {
			var select = '<select ';
			for (i in attributes) {
				select += i+'="'+attributes[i]+'" ';
			}
			select += '>';
			for (i in options) {
				select += '<option value="'+i+'"';
				if (selected != undefined && selected == i) {
					select += ' selected="selected"';
				}
				select += '>'+options[i]+'</option>';
			}
			select += '</select>';
			return select;
		}
	}
	
	jQuery(document).ready(function($){
		
		$("#<?=$module_name?>_settings_content").accordion({
			autoHeight: false,
			header: "h3.accordion",
			active: $('h3[data-hash='+$.<?=$module_name?>CP.currentSection()+']'),
			changestart: function(e,ui){
				window.location.hash = $(ui.newHeader).attr('data-hash');
			}
		});
		$('select.channels').each(function(){
			$.<?=$module_name?>CP.updateSelect(this, $.<?=$module_name?>CP.channels);
		});
 		
		$.<?=$module_name?>CP.checkSelectedChannel('#select_orders', ".requires_orders_channel"); 
		$('#select_orders').bind('change', function(){
			$.<?=$module_name?>CP.checkSelectedChannel('#select_orders', ".requires_orders_channel"); 
		});
		$('select.channels').bind('change', function(){
			var channel_id = Number($(this).val());
			var section = $(this).attr('id').replace('select_', '');
			$('select.field_'+section).children().not('.blank').remove();
 			if ($(this).val() != "")
			{
				for (i in $.<?=$module_name?>CP.fields[channel_id])
				{
					$('select.field_'+section).append('<option value="'+$.<?=$module_name?>CP.fields[channel_id][i].field_id+'">'+$.<?=$module_name?>CP.fields[channel_id][i].field_label+'</option>');
				}
 
			}
		});
		
		$('#<?=$module_name?>_tab').val($.<?=$module_name?>CP.currentSection());

		$('fieldset.plugin_add_new_setting a').bind('click', function(){
			var name = $(this).attr('id').replace('add_new_', '');
			var count = ($('tr.'+name+'_setting:last').length > 0) ? Number($('tr.'+name+'_setting:last').attr('id').replace(name+'_setting_','')) + 1 : 0;
			var plugin_classname = $('#'+name+'_blank').parent().parent().attr('class');
 
			var element = $('#'+name+'_blank').attr('class').split(" ");
			var setting_short_name = element[0];
			
			var clone = $('#'+name+'_blank').clone();
			clone.attr({'id':name+'_setting_'+count});
			clone.attr({'class':name+'_setting'});
			clone.attr({'rel': plugin_classname+'_settings['+setting_short_name+']'});
			clone.find(':input').each(function(){
				var matrix_setting_short_name = $(this).parent().attr('class');
				$(this).parent().attr('rel', matrix_setting_short_name);
				
				$(this).attr('name', plugin_classname+'_settings['+setting_short_name+']['+count+']['+matrix_setting_short_name+']');	
			});
 			clone.children('td').attr('class','');
			$(this).parent().prev().find('tbody').append(clone);
			return false;
		});
 
		$('a.remove_matrix_row').live('click', function(){
			if (confirm('Are you sure you want to delete this row?'))
			{
				if ($(this).parent().get(0).tagName.toLowerCase() == 'td')
				{
					$(this).parent().parent().remove();
				}
				else
				{
					$(this).parent().remove();
				}
			}
			return false;
		}).live('mouseover', function(){
			$(this).find('img').animate({opacity:1});
			console.log('in');
		}).live('mouseout', function(){
			console.log('out');
			$(this).find('img').animate({opacity:.2});
		}).find('img').css({opacity:.2});

 		
		$('.add_matrix_row').bind('click', function(){
			var name = $(this).attr('id').replace('_button', '');
			var index = ($('.'+name+'_row:last').length > 0) ? Number($('.'+name+'_row:last').attr('id').replace(name+'_row_','')) + 1 : 0;
			var clone = $('#'+name+'_row_blank').clone(); 
			clone.attr('id', name+'_row_'+index).addClass(name+'_row').show();
			clone.find(':input').bind('each', function(){
				$(this).attr('name', $(this).attr('data-hash').replace('INDEX', index));
			});
			$(this).parent().before(clone);
			return false;
		});
		
		// Return a helper with preserved width of cells
		var fixHelper = function(e, ui) {
			ui.children().each(function() {
				$(this).width($(this).width());
			});
			return ui;
		};

		$("div.matrix table tbody").sortable({
			helper: fixHelper,
			stop: function(event, ui) { 
				var count=0; 
				$("div.matrix table tbody tr").each(function(){
					$(this).find(':input').each(function(){
 						$(this).attr('name', $(this).parents('tr').attr('rel')+'['+count+']['+$(this).parent().attr('rel')+']');	
					}); 
					count +=1; 
				});
			}
		});

		
	});
		var channels = new Array();
		var channel_fields = new Array();
 
	<?php foreach ($channel_titles as $channel_id => $blog_title) : ?>
		channels[<?php echo $channel_id; ?>] = "<?php echo str_replace("'", "&#39;", $blog_title); ?>";
	<?php endforeach; ?>

	<?php foreach ($fields as $key => $value) : ?>
		channel_fields[<?php echo $key; ?>] = new Array();

		<?php foreach ($value as $count => $field_data) : ?>
		channel_fields[<?php echo $key; ?>][<?php echo $count; ?>] = ['<?php echo $field_data['field_id']; ?>', '<?php echo $field_data['field_name']; ?>', '<?php echo str_replace("'", '&#39;', $field_data['field_label']); ?>'];
		<?php endforeach; ?>

	<?php endforeach; ?>
</script>