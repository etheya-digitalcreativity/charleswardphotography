<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property CI_Controller $EE
 */
class Cartthrob_item_options_upd
{ 
	public $module_name = "cartthrob_item_options"; 
	private $mod_actions = array(
	);
	private $mcp_actions = array(
	);
	private $fieldtypes = array(
	);
	private $hooks = array(
		array('cartthrob_get_all_price_modifiers'),
		array('cartthrob_add_settings_nav'),
 	);
	public $version;
	public $current;
	/**
	 * Tables
	 *
	 * List of custom tables to be used with table_model->update_tables() on install/update
	 *
	 * Notes about field attributes:
	 * -use an int, not a string, for constraint
	 * -use custom attributes, key and primary_key set to TRUE
	 * -don't set null => false unneccessarily
	 * -default values MUST be strings
	 *
	 * @var array 
	 */
	private $tables = array(
 		'cartthrob_item_options_settings' => array(
			'site_id' => array(
				'type' => 'int',
				'constraint' => 4,
				'default' => '1',
			),
			'`key`' => array(
				'type' => 'varchar',
				'constraint' => 255,
			),
			'value' => array(
				'type' => 'text',
				'null' => TRUE,
			),
			'serialized' => array(
				'type' => 'int',
				'constraint' => 1,
				'null' => TRUE,
			),
		),
		'cartthrob_item_options_options' => array(
			'id' => array(
				'type' => 'int',
				'constraint' => 10,
				'auto_increment' => TRUE,
				'primary_key' => TRUE,
			),
			'label' => array(
				'type' => 'varchar',
				'constraint' => 255,
			),
			'short_name' => array(
				'type' => 'varchar',
				'constraint' => 255,
			),
			'data' => array(
				'type' => 'text',
				'null' => TRUE,
			), // serialized data. 
		),
 
	);

     
	public function __construct()
	{ 
		$this->EE =& get_instance();
		
		include PATH_THIRD.$this->module_name.'/config'.EXT;
		
		$this->version = $config['version'];
		
		$this->EE->load->add_package_path(PATH_THIRD.$this->module_name.'/');
		$this->EE->load->add_package_path(PATH_THIRD.'cartthrob/');
		
	}
	
	public function install()
	{
		//install module to exp_modules
		$data = array(
			'module_name' => ucwords($this->module_name),
			'module_version' => $this->version,
			'has_cp_backend' => 'y',
			'has_publish_fields' => 'n'
		);

		$this->EE->db->insert('modules', $data);
		
		//install extension
		
		$this->EE->load->dbforge();
		
		//create tables from $this->tables array
		$this->EE->load->model('table_model');
		
		$this->EE->table_model->update_tables($this->tables);
		
		//check for Addon actions in the database
		//so we don't get duplicates
		$this->EE->db->select('method')
				->from('actions')
				->like('class', ucwords($this->module_name), 'after');
		
		$existing_methods = array();
		
		foreach ($this->EE->db->get()->result() as $row)
		{
			$existing_methods[] = $row->method;
		}
		
		if (!empty($this->hooks))
		{
			foreach ($this->hooks as $row)
			{
				$this->EE->db->insert(
					'extensions',
					array(
						'class' => ucwords($this->module_name).'_ext',
						'method' => $row[0],
						'hook' => ( ! isset($row[1])) ? $row[0] : $row[1],
						'settings' => ( ! isset($row[2])) ? '' : $row[2],
						'priority' => ( ! isset($row[3])) ? 10 : $row[3],
						'version' => $this->version,
						'enabled' => 'y',
					)
				);
			}
		}

		if (!empty($this->mod_actions))
		{
			//install the module actions from $this->mod_actions
			foreach ($this->mod_actions as $method)
			{
				if ( ! in_array($method, $existing_methods))
				{
					$this->EE->db->insert('actions', array('class' => ucwords($this->module_name), 'method' => $method));
				}
			}
			
		}
		
		if (!empty($this->mcp_actions))
		{
			//install the module actions from $this->mcp_actions
			foreach ($this->mcp_actions as $method)
			{
				if ( ! in_array($method, $existing_methods))
				{
					$this->EE->db->insert('actions', array('class' => ucwords($this->module_name).'_mcp', 'method' => $method));
				}
			}
		}

		
		
		if (!empty($this->fieldtypes))
		{
			//install the fieldtypes
			require_once APPPATH.'fieldtypes/EE_Fieldtype'.EXT;

			foreach ($this->fieldtypes as $fieldtype)
			{
				require_once PATH_THIRD.$fieldtype.'/ft.'.$fieldtype.EXT;

				$ft = get_class_vars(ucwords($fieldtype.'_ft'));

				$this->EE->db->insert('fieldtypes', array(
					'name' => $fieldtype,
					'version' => $ft['info']['version'],
					'settings' => base64_encode(serialize(array())),
					'has_global_settings' => method_exists($fieldtype, 'display_global_settings') ? 'y' : 'n'
				));
			}
			
		}

		return TRUE;
	}
	
	public function update($current = '')
	{
		$this->current = $current;
		
		if ($this->current == $this->version)
		{
			return FALSE;
		}
		
		$this->EE->db->update('extensions', array('version' => $this->version), array('class' => ucwords($this->module_name).'_ext'));
		
		$this->EE->load->dbforge();
		
		//check for Addon actions in the database
		//so we don't get duplicates
		$this->EE->db->select('method')
				->from('actions')
				->like('class', ucwords($this->module_name), 'after');

		$existing_methods = array();

		foreach ($this->EE->db->get()->result() as $row)
		{
			$existing_methods[] = $row->method;
		}
		
		$this->EE->load->model('table_model');
		
		$this->EE->table_model->update_tables($this->tables);

		//install the module actions from $this->mod_actions
		foreach ($this->mod_actions as $method)
		{
			if ( ! in_array($method, $existing_methods))
			{
				$this->EE->db->insert('actions', array('class' => ucwords($this->module_name), 'method' => $method));
			}
		}
		
		return TRUE;
	}
	
	public function uninstall()
	{
		$this->EE->db->delete('modules', array('module_name' => ucwords($this->module_name)));
		
		$this->EE->db->like('class', ucwords($this->module_name), 'after')->delete('actions');
		
		$this->EE->db->delete('extensions', array('class' => ucwords($this->module_name).'_ext'));
		
		//should we do this?
		//nah, do it yourself if you really want to
		/*
		$this->EE->load->dbforge();

		foreach (array_keys($this->tables) as $table)
		{
			$this->EE->dbforge->drop_table($table);
		}
		*/
		
		return TRUE;
	}
	
	private function older_than($version)
	{
		return version_compare($this->current, $version, '<');
	}
}

