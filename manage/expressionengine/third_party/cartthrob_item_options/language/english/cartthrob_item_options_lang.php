<?php 
$lang = array(
 	'cartthrob_item_options_module_name'		=> 'Carthrob Global Item Options', 
	'cartthrob_item_options_module_description'		=> 'CartThrob Shopping Cart Global Item Options', 
	'nav_global_item_options'					=> 'Global Options',
 	'nav_head_view'								=> 'View All',
	'cartthrob_item_options_label_view'			=> 'Option Group',
	'cartthrob_item_options_short_name_view'	=> 'Short Name',

	'nav_head_add'								=> 'Add Option Group',
	'nav_head_cartthrob_item_options_system_settings'	=> 'Channel Settings',
	'nav_head_system_settings'		=> 'Channel Settings',
	'system_settings_header'		=> 'Channel Settings',
	'cartthrob_item_options_channels'	=> 'Product Channels',
	
	
	'cartthrob_item_options_updated'				=> 'Option Updated',
	'view_header'								=> 'Option Groups',
	'cartthrob_item_options_label'				=> 'Item Option Group Label (descriptive)',
	'cartthrob_item_options_short_name'			=> 'short_name (used in templates). <strog>NOTE:</strong> It is important that this field be named uniquely. If it is named the same as another option group, or another Channel Field "short_name", problems can occur. It may be simpler to prefix all globally available options with "global_" (eg. global_shirt_sizes). ',
	'cartthrob_item_options_actions'			=> '',
	'add_header'								=> 'Add option group',
	'cartthrob_item_options_add'				=> 'Option Group Information',
	'cartthrob_item_options_add_description'	=> '',
	'cartthrob_item_options_data_title'			=> 'Options List',
	'cartthrob_item_options_data_overview'		=> 'Add an option_value (sku value--all skus in the list should be unique within this group), option_name (descriptive value), and a price (optional) for each option below. The items added below will be available globally to all products. ', 
	
	'cartthrob_item_options_system_settings_title'=> 'Channel Settings',
	
	'delete_when_checked'						=> 'If checked, this item will be deleted',
	'delete_header'								=> 'Delete',
	
	'cartthrob_item_options_delete'				=> 'Delete this option group?',
	'cartthrob_item_options_delete_description'	=> '',
	'cartthrob_item_options_deleted'			=> 'Item successfully deleted',
	'option_value'								=> 'option_value (alpha-numeric + underscores + hyphens)', 
	'option_name'								=> 'option_name (descriptive)',
	'price'										=> 'price  (eg. 10.00)', 
	
	'edit_header'								=> 'Edit Option Group',
	'cartthrob_item_options_edit'				=> 'Group Settings',
	'cartthrob_item_options_edit_description'	=> '',
	'cartthrob_item_options_none'				=> 'There are no option groups. Please add one',
	'nav_head_delete'							=> 'Delete',
	'nav_head_edit'								=> 'Edit',
	
);