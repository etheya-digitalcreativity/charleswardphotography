<?php

// get the version from config.php
require PATH_THIRD.'wyvern_video/config.php';

$lang = array(

"wyvern_video_module_name" => $config['name'],
"wyvern_video_module_description" => $config['description'],

"vimeo_consumer_key" => "Vimeo Application Key",
"vimeo_consumer_secret" => "Vimeo Application Secret",

"wyvern_video_settings_saved" => "Settings Saved!",

"show_details" => "Show video details on Publish page?",

"global_width"  => "<b>Default Video Width</b>",
"global_height" => "<b>Default Video Height</b>",

"field_width" => "Video Width",
"field_height"  => "Video Height",
"allow_resize" => "Allow changing of video dimensions in the publish page?",

"settings_global"   => "Global Settings",
"settings_global_desc" => "",

"settings_youtube"    => "YouTube Settings",
"settings_youtube_desc" => "",

"settings_vimeo"    => "Vimeo Settings",
"settings_vimeo_desc" => "",

"show_on_load" => "<b>Show videos from this user on load?</b><p>When the video dialog opens it will automatically display all videos from this user's account.</p>",

"user" => "<b>Username</b><p>Enter the Username or Account name to limit your searches to. This filter can be removed in the search listing.</p>",
// "settings_vimeo_desc" => "In order to use Vimeo you need to have an existing application, which I've already done for you. If you would like to create your own application feel free to do so. Login to your Vimeo account and go to <a href=\"http://vimeo.com/api/applications\">http://vimeo.com/api/applications</a> and follow the instructions, then enter your credentials here."

);