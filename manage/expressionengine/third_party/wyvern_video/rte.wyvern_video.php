<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once PATH_THIRD.'wyvern_video/config.php';

class Wyvern_video_rte {
    
    public $info = array(
        'name'          => WYVERN_VIDEO_NAME,
        'version'       => WYVERN_VIDEO_VERSION,
        'description'   => WYVERN_VIDEO_DESCRIPTION,
        'cp_only'       => 'n'
    );
    
    private $EE;

    public function __construct()
    {
        $this->EE =& get_instance();

        // Create cache
        if (! isset($this->EE->session->cache[WYVERN_VIDEO_SHORT_NAME]))
        {
            $this->EE->session->cache[WYVERN_VIDEO_SHORT_NAME] = array();
        }
        $this->cache =& $this->EE->session->cache[WYVERN_VIDEO_SHORT_NAME];

        if (! isset($this->EE->wyvern_video_helper))
        {
            require PATH_THIRD.'wyvern_video/helper.wyvern_video.php';
            $this->EE->wyvern_video_helper = new Wyvern_video_helper;
        }
    }
    
    // --------------------------------------------------------------------

    public function globals()
    {
        $this->EE->lang->loadfile('rte');
        return array(
            'rte.wyvern_video'    => array(
                'add'       => 'Video',
                'remove'    => 'Video'
            )
        );
    }

    // --------------------------------------------------------------------

    public function definition()
    {
        return 'WysiHat.addButton("wyvern_video", {
            label:   "Video",
            handler: function(state, finalize) {
                WyvernVideo.Field.init(WyvernVideo.dialog, {editor: this, editor_state: state}, true);
            },
            init: function(name, $element) {
                this.parent.init(name, $element);
                WyvernVideo.Handler.init(this, $element);
                return this;
            }
        });';
    }
}