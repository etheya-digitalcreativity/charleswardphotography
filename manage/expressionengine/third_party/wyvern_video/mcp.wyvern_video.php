<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * ExpressionEngine Wyvern Video Module Class
 *
 * @package     ExpressionEngine
 * @subpackage  Modules
 * @category    Wyvern Video
 * @author      Brian Litzinger
 * @copyright   Copyright (c) 2010, 2011 - Brian Litzinger
 * @link        http://boldminded.com/add-ons/wyvern-video
 * @license 
 *
 * Copyright (c) 2011, 2012. BoldMinded, LLC
 * All rights reserved.
 *
 * This source is commercial software. Use of this software requires a
 * site license for each domain it is used on. Use of this software or any
 * of its source code without express written permission in the form of
 * a purchased commercial or other license is prohibited.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 *
 * As part of the license agreement for this software, all modifications
 * to this source must be submitted to the original author for review and
 * possible inclusion in future releases. No compensation will be provided
 * for patches, although where possible we will attribute each contribution
 * in file revision notes. Submitting such modifications constitutes
 * assignment of copyright to the original author (Brian Litzinger and
 * BoldMinded, LLC) for such modifications. If you do not wish to assign
 * copyright to the original author, your license to  use and modify this
 * source is null and void. Use of this software constitutes your agreement
 * to this clause.
 */

require_once PATH_THIRD.'wyvern_video/config.php';

class Wyvern_video_mcp {
    
    function __construct()
    {
        $this->EE =& get_instance();

        if (! isset($this->EE->wyvern_video_helper))
        {
            require PATH_THIRD.'wyvern_video/helper.wyvern_video.php';
            $this->EE->wyvern_video_helper = new Wyvern_video_helper;
        }

        $this->base_url = BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=wyvern_video';

        $msg = $this->EE->session->flashdata('message_success');

        if ($msg)
        {
            $this->_destroy_notice();
        }
        
        // $this->menu_title = $query->row('setting_value') ? $query->row('setting_value') : $this->EE->lang->line('super_globals_module_name');
        $this->EE->cp->set_variable('cp_page_title', WYVERN_VIDEO_NAME);
    }
    
    public function index() 
    {
        $this->EE->load->library('table');
        $this->EE->load->helper('path');
        $this->EE->load->helper('form');

        $vars['action_url'] = 'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=wyvern_video'.AMP.'method=save_settings';
        $vars['hidden'] = NULL;
        $vars['files'] = array();
        $vars['morphine'] = array_key_exists('nsm_morphine', $this->EE->addons->get_installed());

        // Defaults
        $vars['fields'] = $this->EE->wyvern_video_helper->default_settings;

        $settings = $this->EE->wyvern_video_helper->get_settings();

        if ($settings)
        {
            foreach ($settings as $group => $data)
            {
                foreach($data as $key => $value)
                {
                    $vars['fields'][$group][$key] = $value;
                }
            }
        }

        return $this->EE->load->view('index', $vars, TRUE);
    }

    public function save_settings()
    {
        $data = $this->EE->input->post('setting');
        $this->EE->wyvern_video_helper->save_settings($data);

        $this->EE->session->set_flashdata('message_success', 'global_settings_saved');
        $this->EE->functions->redirect($this->base_url);
        exit;
    }

    private function _cp_menu()
    {
        $default = array(
            'menu_home' => $this->base_url
        );
        
        $this->EE->cp->set_right_nav($default);
    }

    /*
        Show EE notification and hide it after a few seconds
    */
    private function _destroy_notice()
    {
        $this->EE->javascript->output(array(
            'window.setTimeout(function(){$.ee_notice.destroy()}, 4000);'
        ));
    }
    
}