<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * ExpressionEngine Wyvern Video Extension Class
 *
 * @package     ExpressionEngine
 * @subpackage  Extensions
 * @category    Wyvern Video
 * @author      Brian Litzinger
 * @copyright   Copyright (c) 2010, 2011 - Brian Litzinger
 * @link        http://boldminded.com/add-ons/wyvern-video
 * @license 
 *
 * Copyright (c) 2011, 2012. BoldMinded, LLC
 * All rights reserved.
 *
 * This source is commercial software. Use of this software requires a
 * site license for each domain it is used on. Use of this software or any
 * of its source code without express written permission in the form of
 * a purchased commercial or other license is prohibited.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 *
 * As part of the license agreement for this software, all modifications
 * to this source must be submitted to the original author for review and
 * possible inclusion in future releases. No compensation will be provided
 * for patches, although where possible we will attribute each contribution
 * in file revision notes. Submitting such modifications constitutes
 * assignment of copyright to the original author (Brian Litzinger and
 * BoldMinded, LLC) for such modifications. If you do not wish to assign
 * copyright to the original author, your license to  use and modify this
 * source is null and void. Use of this software constitutes your agreement
 * to this clause.
 */
 
require_once PATH_THIRD.'wyvern_video/config.php';

class Wyvern_video_ext {

    public $settings       = array();
    public $name           = WYVERN_VIDEO_NAME;
    public $version        = WYVERN_VIDEO_VERSION;
    public $description    = '';
    public $settings_exist = 'n';
    public $docs_url       = 'http://boldminded.com/add-ons/wyvern-video';
    public $required_by    = array('module');
    public $cache;

    private $iframe_regex = '/<iframe.*?data-video-type="(.*?)".*?><\/iframe>/';
    private $image_regex  = '/<img.*?data-cke-realelement="(.*?)".*?>/';

    /**
     * Constructor
     */
    public function __construct($settings = '') 
    {
        $this->EE =& get_instance();
        
        // Create cache
        if (! isset($this->EE->session->cache[WYVERN_VIDEO_SHORT_NAME]))
        {
            $this->EE->session->cache[WYVERN_VIDEO_SHORT_NAME] = array();
        }
        $this->cache =& $this->EE->session->cache[WYVERN_VIDEO_SHORT_NAME];

        if (! isset($this->EE->wyvern_video_helper))
        {
            require PATH_THIRD.'wyvern_video/helper.wyvern_video.php';
            $this->EE->wyvern_video_helper = new Wyvern_video_helper;
        }
    }
    
    public function activate_extension()
    {
        return TRUE;  
    }

    public function update_extension($current = '')
    {
        return TRUE;
    }
    
    public function disable_extension() 
    {
        return TRUE;
    }
    
    public function sessions_end($session)
    {
    }

    public function entry_submission_absolute_end($entry_id, $meta, $data)
    {
        $rte_fields = $this->EE->wyvern_video_helper->get_rte_fields();
        
        foreach ($data as $field_name => $field_value)
        {
            $new_data = array();

            // See if its an RTE field
            if (in_array($field_name, $rte_fields))
            {
                preg_match_all($this->image_regex, $field_value, $matches, PREG_SET_ORDER);

                foreach ($matches as $match)
                {
                    $image = $match[0];

                    // Do we have an iframe(s)? If so find them, and grab the attributes.
                    list($args, $image) = $this->EE->wyvern_video_helper->get_args($image, $this->image_regex);
                    $params = $this->EE->wyvern_video_helper->assign_parameters($args);

                    if ($params)
                    {
                        $iframe = rawurldecode($params['data-cke-realelement']);

                        preg_match($this->iframe_regex, $iframe, $matches);

                        // If false/blank then we don't need to add these attributes as the iframe element already has them.
                        if ( ! isset($matches[1]) OR $matches[1] == '')
                        {
                            // Make sure our attributes remain on the iframe tag. Save the thumb too as it will avoid an extra API lookup in publish_form_entry_data
                            $attr = 'data-video-thumbnail="'. $params['src'] .'" data-video-type="'. $params['data-video-type'] .'" data-video-id="'. $params['data-video-id'] .'" data-query="'. $params['data-query'] .'"';

                            // Add attr
                            $iframe = preg_replace('/(src=".*?")/', "$1 $attr", $iframe);
                        }

                        // Swap out the image entirely with our iframe
                        $field_value = str_replace($image, $iframe, $field_value);
                    }
                }

                // Because we can't manipulate the $_POST data prior to EE using it
                $this->EE->wyvern_video_helper->insert_or_update('channel_data', array($field_name => $field_value), array(
                    'entry_id' => $entry_id
                ));
            }
        }
    }

    public function publish_form_entry_data($data)
    {
        $this->EE->wyvern_video_helper->load_assets();

        $rte_fields = $this->EE->wyvern_video_helper->get_rte_fields();

        foreach ($data as $field_name => $field_value)
        {
            // See if its an RTE field
            if (in_array($field_name, $rte_fields))
            {
                preg_match_all($this->iframe_regex, $field_value, $matches, PREG_SET_ORDER);

                foreach ($matches as $match)
                {
                    $iframe = $match[0];

                    // Do we have an iframe(s)? If so find them, and grab the attributes.
                    list($args, $iframe) = $this->EE->wyvern_video_helper->get_args($iframe, $this->iframe_regex);
                    $params = $this->EE->wyvern_video_helper->assign_parameters($args);

                    if ($params)
                    {
                        // @todo - need method to lookup thumbnails
                        $params['src'] = isset($params['data-video-thumbnail']) ? $params['data-video-thumbnail'] : '';
                        $params['data-cke-realelement'] = rawurlencode($iframe);

                        $image = '<img ';

                        foreach ($params as $pk => $pv)
                        {
                            $image .= $pk .'="'. $pv .'" ';
                        }

                        $image = trim(substr($image, 0, -1));

                        $image .= '>';

                        // Swap out the iframe entirely with our image
                        $field_value = str_replace($iframe, $image, $field_value);
                    }
                }

                $data[$field_name] = $field_value;
            }
        }

        return $data;
    }

    public function wygwam_config($config, $settings)
    {
        if (isset($config['extraPlugins']) AND strstr($config['extraPlugins'], 'video') !== FALSE)
        {
            $config['toolbar'][]  = array('Video');    
        }
        
        return $config;
    }
}