<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * ExpressionEngine Wyvern Video Fieldtype Class
 *
 * @package     ExpressionEngine
 * @subpackage  Fieldtypes
 * @category    Wyvern Video
 * @author      Brian Litzinger
 * @copyright   Copyright (c) 2010, 2011 - Brian Litzinger
 * @link        http://boldminded.com/add-ons/wyvern-video
 * @license 
 *
 * Copyright (c) 2011, 2012. BoldMinded, LLC
 * All rights reserved.
 *
 * This source is commercial software. Use of this software requires a
 * site license for each domain it is used on. Use of this software or any
 * of its source code without express written permission in the form of
 * a purchased commercial or other license is prohibited.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 *
 * As part of the license agreement for this software, all modifications
 * to this source must be submitted to the original author for review and
 * possible inclusion in future releases. No compensation will be provided
 * for patches, although where possible we will attribute each contribution
 * in file revision notes. Submitting such modifications constitutes
 * assignment of copyright to the original author (Brian Litzinger and
 * BoldMinded, LLC) for such modifications. If you do not wish to assign
 * copyright to the original author, your license to  use and modify this
 * source is null and void. Use of this software constitutes your agreement
 * to this clause.
 */

require_once PATH_THIRD.'wyvern_video/config.php';
 
class wyvern_video_ft extends EE_Fieldtype {

    public $settings = array();
    public $has_array_data = FALSE;
    public $info = array(
        'name'      => WYVERN_VIDEO_NAME,
        'version'   => WYVERN_VIDEO_VERSION
    );
    
    private $site_id;
    private $cache = array();

    private $attr_display = array(
        'url',
        'title',
        'description'
    );
    
    /**
     * Constructor
     *
     * @access  public
     */
    function __construct()
    {
        parent::__construct();
        
        $this->site_id = $this->EE->config->item('site_id');
        
        // Create cache
        if (! isset($this->EE->session->cache[WYVERN_VIDEO_SHORT_NAME]))
        {
            $this->EE->session->cache[WYVERN_VIDEO_SHORT_NAME] = array();
        }
        $this->cache =& $this->EE->session->cache[WYVERN_VIDEO_SHORT_NAME];

        if (! isset($this->EE->wyvern_video_helper))
        {
            require PATH_THIRD.'wyvern_video/helper.wyvern_video.php';
            $this->EE->wyvern_video_helper = new Wyvern_video_helper;
        }

        $this->settings = $this->EE->wyvern_video_helper->get_settings();
    }

    function validate($data)
    {
        return TRUE;
    }

    /**
     * Default field display
     */
    public function display_field($data, $cell_name = false, $col_id = false)
    {
        $this->EE->wyvern_video_helper->load_assets();

        $settings = isset($this->settings['wyvern_video']) ? $this->settings['wyvern_video'] : array();
        $global_settings = $this->EE->wyvern_video_helper->get_settings();

        $disabled = 'disabled';
        $add_btn_text = 'Add Video';

        // Make sure we have data, and a video ID first.
        if ($data AND ! is_array($data))
        {
            $data = json_decode( html_entity_decode($data, ENT_COMPAT, 'UTF-8') );

            if (isset($data->id) AND $data->id != '')
            {
                $disabled = '';
                $add_btn_text = 'Change Video';
            }
        }

        // Normal field or Matrix cell?
        $field_name = $cell_name ? $cell_name : $this->field_name;
        $field_name_config = $col_id ? 'col_id_'.$col_id : $field_name;

        // Clean it up
        $field_name_clean = str_replace(array('[',']'), '', $field_name);

        $html = '';
        $video_info = '';

        // If Assets is installed, use its button style for consistency
        if (array_key_exists('assets', $this->EE->addons->get_installed()))
        {
            $html .= '<div class="assets-field-btns">
                <a class="wyvern-video-field-btn-'. $field_name .' wyvern-video-field-btn-add wyvern-video-field-btn-add-'. $field_name .' assets-btn assets-btn-big assets-add" rel="'. $field_name_clean .'" data-field-config="'. $field_name_config .'"><span></span>'. $add_btn_text .'</a>
                <a class="wyvern-video-field-btn-'. $field_name .' wyvern-video-field-btn-remove wyvern-video-field-btn-remove-'. $field_name .' assets-btn assets-'. $disabled .' assets-btn-big assets-remove" rel="'. $field_name_clean .'" data-field-config="'. $field_name_config .'"><span></span>Remove Video</a>
            </div>';
        }
        // Otherwise, use a native EE button style
        else
        {
            $html .= '<div class="rightNav">
                <div class="wyvern-video-field-btn-'. $field_name .' button"><a class="submit wyvern-video-field-btn-add wyvern-video-field-btn-add-'. $field_name .'" rel="'. $field_name_clean .'" data-field-config="'. $field_name_config .'"><span></span>'. $add_btn_text .'</a></div>
                <div class="wyvern-video-field-btn-'. $field_name .' button"><a class="'. $disabled .' submit wyvern-video-field-btn-remove wyvern-video-field-btn-remove-'. $field_name .'" rel="'. $field_name_clean .'" data-field-config="'. $field_name_config .'"><span></span>Remove Video</a></div>
                <div style="clear: both;"></div>
            </div>';
        }

        foreach ($this->EE->wyvern_video_helper->attributes as $attr => $value)
        {
            $html .= '<input type="hidden" data-field-name="'. $field_name_clean .'" data-attr-name="'. $attr .'" class="wyvern-video-field-data-attr wyvern-video-field-data-attr-'. $field_name_clean .' wyvern-video-field-data-attr-'. $attr .'-'. $field_name_clean .'" value="'. (isset($data->$attr) ? $data->$attr : '') .'" name="'. $field_name .'['. $attr .']" />';

            if (in_array($attr, $this->attr_display) AND isset($data->$attr) AND $data->$attr != '')
            {
                $video_info .= '<tr>
                    <th width="30%">'. ucwords($attr) .'</th>
                    <td width="70%">'. (isset($data->$attr) ? $data->$attr : '') .'</td>
                </tr>';
            }
        }

        $iframe = $this->EE->wyvern_video_helper->get_iframe($data);
        $info_td = '';
        $has_info = '';

        if (isset($settings['show_details']) AND $settings['show_details'] == 'yes')
        {
            $has_info = ' has_info';
            $info_td = '<td class="info"><table>'. $video_info .'</table></td>';
        }

        if ( ! isset($data->id) OR $data->id == '')
        {
            $field = '<div class="wyvern-video-preview wyvern-video-preview-empty wyvern-video-preview-'. $field_name_clean .'"></div>' . $html;
        }
        else if (isset($data->id) AND $data->id != '')
        {
            $field = '<div class="wyvern-video-preview wyvern-video-preview-'. $field_name_clean .'">
                <table class="wyvern-video-field-attr-display" width="'. (isset($data->width) ? $data->width : '360') .'">
                    <tr>
                        <td class="iframe'. $has_info .'">'. $iframe .'</td>
                        '. $info_td .'
                    </tr>
                </table>
            </div>' . $html;
        }
        else
        {
            $field = '<div class="wyvern-video-preview wyvern-video-preview-'. $field_name_clean .'"></div>' . $html;
        }

        // Add our settings for this field to a global object.
        $script = '
            if (typeof wyvern_config.wyvern_video.'. $field_name_config .' == "undefined") {
                wyvern_config.wyvern_video.'. $field_name_config .' = {};
            }

            wyvern_config.wyvern_video.'. $field_name_config .'.settings = {
                show_details: "'. (isset($settings['show_details']) ? $settings['show_details'] : 'y') .'",
                video_width: '. (isset($settings['field_width']) ? $settings['field_width'] : $global_settings['settings_global']['global_width']) .',
                video_height: '. (isset($settings['field_height']) ? $settings['field_height'] : $global_settings['settings_global']['global_height']) .',
                allow_resize: "'. (isset($settings['allow_resize']) ? $settings['allow_resize'] : 'no') .'"
            };';
            
        $this->EE->javascript->output($script);
        $this->EE->javascript->compile();

        return $field;
    }

    /**
     * Matrix cell display
     */
    public function display_cell($data)
    {
        $r['class'] = 'wyvern-video-matrix';
        $r['data'] = $this->display_field($data, $this->cell_name, $this->col_id);

        $this->EE->wyvern_video_helper->load_js_matrix();

        return $r;
    }

    /*
     * Low Variables Fieldtype Display
     */
    function display_var_field($data)
    {
        return $this->display_field($data, 'var['. $this->var_id .']');
    }

    /*
     * Save normal field
     */
    public function save($data)
    {
        if (isset($data['id']) AND $data['id'] != '')
        {
            return json_encode($data);
        }
        else
        {
            return '';
        }
    }

    /*
     * Save for Matrix
     */
    public function save_cell($data)
    {
        return $this->save($data);
    }

    /*
     * Save Low Variables field
     */
    public function save_var_field($data)
    {
        return $this->save($data);
    }

    /*
     * Display normal individual field settings
     */
    function display_settings($data)
    {
        $rows = $this->EE->wyvern_video_helper->get_field_settings_options($this, $data);

        foreach ($rows as $row)
        {
            $this->EE->table->add_row($row[0], $row[1]);
        }
    }
    
    /**
     * Display Matrix Cell Settings
     */
    function display_cell_settings($data)
    {
        return $this->EE->wyvern_video_helper->get_field_settings_options($this, $data, true);
    }
    
    /**
     * Display Low Variables Settings
     */
    function display_var_settings($data)
    {
        return $this->EE->wyvern_video_helper->get_field_settings_options($this, $data);
    }

    /*
     * Save individual field settings
     */
    function save_settings($settings)
    {
        $data = $this->EE->input->post('wyvern_video');
        $global_settings = $this->EE->wyvern_video_helper->get_settings();

        $settings['wyvern_video']['show_details'] = isset($data['show_details']) ? $data['show_details'] : 'no';
        $settings['wyvern_video']['field_width'] = isset($data['field_width']) ? $data['field_width'] : $global_settings['settings_global']['global_width'];
        $settings['wyvern_video']['field_height'] = isset($data['field_height']) ? $data['field_height'] : $global_settings['settings_global']['global_height'];
        $settings['wyvern_video']['allow_resize'] = isset($data['allow_resize']) ? $data['allow_resize'] : 'no';
        
        return $settings;
    }

    /**
     * Save Matrix Cell Settings
     */
    function save_cell_settings($settings)
    {
        $global_settings = $this->EE->wyvern_video_helper->get_settings();

        return array(
            'wyvern_video' => array(
                'show_details'  => isset($settings['wyvern_video']['show_details']) ? $settings['wyvern_video']['show_details'] : 'no',
                'field_width'  => isset($settings['wyvern_video']['field_width']) ? $settings['wyvern_video']['field_width'] : $global_settings['settings_global']['global_width'],
                'field_height'  => isset($settings['wyvern_video']['field_height']) ? $settings['wyvern_video']['field_height'] : $global_settings['settings_global']['global_height'],
                'allow_resize'  => isset($settings['wyvern_video']['allow_resize']) ? $settings['wyvern_video']['allow_resize'] : 'no'
            )
        );
    }
    
    /**
     * Save Low Variables Settings
     */
    function save_var_settings($settings)
    {
        return $this->save_settings($settings);
    }

    /**
     * Template display functions
     */
    public function pre_process($data)
    {
        if ($data)
        {
            $data = json_decode( html_entity_decode($data, ENT_COMPAT, 'UTF-8') );
        }

        return $data;
    }

    public function replace_tag($data, $params = array())
    {
        if (isset($params['protocol']) AND $params['protocol'] == 'https')
        {
            return isset($data->embed) ? $this->EE->wyvern_video_helper->make_url(urldecode($data->embed), 'https') : '';
        }
        else
        {
            return isset($data->embed) ? $this->EE->wyvern_video_helper->make_url(urldecode($data->embed)) : '';
        }
    }

    /**
     * Low Variables tag parsing
     */
    public function display_var_tag($data, $params, $tagdata)
    {
        $data = $this->pre_process($data);

        // We have a pair... (and hopefuly the user actually used the :pair method)
        if ($tagdata)
        {
            if (isset($params['variable_prefix']))
            {
                $prefix = $params['variable_prefix'];
                $prefixed_data = array();

                foreach($data as $k => $v)
                {
                    $prefixed_data[$prefix.$k] = $v;
                }

                $data = $prefixed_data;
            }

            // This should always be present, but just incase
            if (isset($data['embed']))
            {
                if (isset($params['protocol']) AND $params['protocol'] == 'https')
                {
                    $data['embed'] = $this->EE->wyvern_video_helper->make_url(urldecode($data->embed), 'https');
                }
                else
                {
                    $data['embed'] = $this->EE->wyvern_video_helper->make_url(urldecode($data->embed));
                }
            }

            return $this->EE->TMPL->parse_variables($tagdata, array($data));
        }
        else
        {
            // See if the user has requested a specific video attribute
            if (isset($params['attribute']) AND isset($data->$params['attribute']))
            {
                return $data->$params['attribute'];
            }
            else
            {
                if (isset($params['protocol']) AND $params['protocol'] == 'https')
                {
                    return $this->EE->wyvern_video_helper->make_url(urldecode($data->embed), 'https');
                }
                else
                {
                    return $this->EE->wyvern_video_helper->make_url(urldecode($data->embed));
                }
            }
        }
    }

    public function replace_url($data)
    {
        return isset($data->url) ? $this->EE->wyvern_video_helper->make_url($data->url) : '';
    }

    public function replace_title($data)
    {
        return isset($data->title) ? $data->title : '';
    }

    public function replace_description($data)
    {
        return isset($data->description) ? $data->description : '';
    }

    public function replace_width($data)
    {
        return isset($data->width) ? $data->width : '';
    }

    public function replace_height($data)
    {
        return isset($data->height) ? $data->height : '';
    }   

    public function replace_id($data)
    {
        return isset($data->id) ? $data->id : '';
    }

    public function replace_author($data)
    {
        return isset($data->author) ? $data->author : '';
    }

    public function replace_length($data, $params)
    {
        if (isset($data->length))
        {
            $stamp = strtotime(date("H:i:s", $data->length));
            
            if (isset($params['format']))
            {
                $format = str_replace('%', '', $params['format']);

                return date($format, $stamp);
            }

            return $stamp;
        }

        return '';
    }

    public function replace_thumbnail_small($data)
    {
        return isset($data->thumbnail_small) ? $data->thumbnail_small : '';
    }

    public function replace_thumbnail_large($data)
    {
        return isset($data->thumbnail_large) ? $data->thumbnail_large : '';
    }

    public function replace_type($data)
    {
        return isset($data->type) ? $data->type : '';
    }
}