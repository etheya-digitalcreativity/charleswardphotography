
<div class="mor">
<div class="tg">
    <?php echo form_open($action_url, 'id="super_globals_module", class="index"', $hidden);

        foreach ($fields as $group => $data)
        {
            echo '<h2>'. lang($group) .'</h2>';

            // If NSM Morphine is installed
            if ($morphine)
            {
                $this->table->set_template(array());
            }
            else
            {
                $this->table->set_template($cp_table_template);
            }

            $this->table->set_heading(array('data' => lang('preference'), 'style' => 'width: 40%'), lang('setting'));
            echo '<p>'. lang($group.'_desc') .'</p>';

            foreach($data as $name => $value)
            {
                $this->table->add_row(
                    array('data' => lang($name)),
                    array('data' => $this->wyvern_video_helper->get_setting_field($name, $value, $group))
                );
            }

            echo $this->table->generate();
            $this->table->clear();
        }
        ?>
        <p class="centerSubmit" id="publish_submit_buttons">
            <?=form_submit(array('name' => 'submit', 'value' => lang('save'), 'class' => 'submit'))?>
        </p>
    </form>
</div>
</div>