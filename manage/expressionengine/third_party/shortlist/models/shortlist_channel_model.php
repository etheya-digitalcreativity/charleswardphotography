<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Shortlist Channel Model class
 *
 * @package         shortlist_ee_addon
 * @version         2.5.3
 * @author          Joel Bradbury ~ <joel@squarebit.co.uk>
 * @link            http://squarebit.co.uk/shortlist
 * @copyright       Copyright (c) 2013, Joel
 */
class Shortlist_channel_model extends Shortlist_model {

	private $shortlist_prefix = 'shortlist_';
	// --------------------------------------------------------------------
	// METHODS
	// --------------------------------------------------------------------

	/**
	 * Constructor
	 *
	 * @access      public
	 * @return      void
	 */
	function __construct()
	{
		// Call parent constructor
		parent::__construct();

		// Initialize this model
		$this->initialize(
			'shortlist_channel',
			'item_id',
			array(
				'site_id'       		=> 'int(4) unsigned NOT NULL',
				'channel_id'			=> 'int(10) unsigned NOT NULL',
				'settings'				=> 'text')
		);
	}

	// --------------------------------------------------------------------

	/**
	 * Installs given table
	 *
	 * @access      public
	 * @return      void
	 */
	public function install()
	{
		// Call parent install
		parent::install();

		// Add indexes to table
		ee()->db->query("ALTER TABLE {$this->table()} ADD INDEX (`site_id`)");

		// Also we need to create EE channels, field groups and fields on install

		// 1. Check we've not already got a channel in place
		ee()->load->library('api');
		ee()->api->instantiate('channel_structure');

		// Field groups first
		$field_group_id = $this->_install_fieldgroup();
		$channel_id = $this->_install_channel($field_group_id );

		// Now create the required fields
		$this->_install_fields( $field_group_id );
	}

	// --------------------------------------------------------------------

	public function uninstall()
	{
		// Call parent uninstall
		parent::uninstall();

		// Remove the channels, fields and field groups we created

		ee()->load->library('api');
		ee()->api->instantiate('channel_structure');

		$this->_uninstall_fields();
		$this->_uninstall_fieldgroup();
		$this->_uninstall_channel();

		return;
	}


	// --------------------------------------------------------------------

	public function find_create( $unique_val = '', $keys = array() )
	{
		if( $unique_val == '' ) return FALSE;

		// Look for this entry in the ee table

		$unique_field_id = $this->_unique_field_id();

		$result = ee()->db->where('field_id_' . $unique_field_id, $unique_val )
						->get('channel_data')
						->result_array();

		if( ! empty( $result ) )
		{
			// Just return the first
			$row = current( $result );

			return $row['entry_id'];
		}

		// No entry, we need to create one
		$entry_id = $this->_create( $unique_val, $keys );

		if( $entry_id === FALSE ) return FALSE;

		return $entry_id;
	}

	// --------------------------------------------------------------------

	private function _create( $unique_val, $keys )
	{
		ee()->load->library('api');
		ee()->load->library('api/Api_channel_entries');

		// Get the channel_id and field_ids for this
		$meta = $this->get_meta();

		$data['author_id'] 			= 1;
		$data['entry_date'] 		= ee()->localize->now;

		foreach( $meta as $key => $row  )
		{
			if( $key == SHORTLIST_UNIQUE_FIELD_NAME )
			{
				$data['field_id_' . $row ] = $unique_val;
			}

			if( $key == SHORTLIST_DATABLOCK_FIELD_NAME )
			{
				$data['field_id_' . $row ] = base64_encode( serialize( $keys ) );
			}
		}

		// Is a 'title' key passed?
		if( isset( $keys['title'] ) ) $data['title'] = $keys['title'];
		elseif( isset( $keys['attr:title'] ) ) $data['title'] = $keys['attr:title'];
		else $data['title'] = $unique_val;

		// We need to temporarily change the user_group for this user, so the
		// channel entries api will let us create this entry
		$current_user_group = ee()->session->userdata('group_id');
		$current_member_id = ee()->session->userdata('member_id');

		ee()->session->userdata['group_id'] = 1;
		ee()->session->userdata['member_id'] = 1;

		if( ee()->api_channel_entries->submit_new_entry( $meta['channel_id'], $data ) === FALSE )
		{
			$this->errors = ee()->api_channel_entries->get_errors();
			return FALSE;
		}

		$entry_id = ee()->api_channel_entries->entry_id;

		// reset their group_id
		ee()->session->userdata['group_id'] = $current_user_group;
		ee()->session->userdata['member_id'] = $current_member_id;

		return $entry_id;
	}


	// --------------------------------------------------------------------

	public function get_meta( $what = 'all')
	{
		// We need the channel_id and field_ids for general usage
		$meta = array();

		if( $what == 'all' )
		{
			$channel = ee()->db->select('channel_id')
						->where('channel_name', SHORTLIST_CHANNEL_NAME)
						->limit(1)
						->get('channels')
						->row_array();

			$meta['channel_id'] = $channel['channel_id'];
		}


		$fields = ee()->db->select( array('field_id', 'field_name' ) )
						->where_in('field_name', array( SHORTLIST_UNIQUE_FIELD_NAME, SHORTLIST_DATABLOCK_FIELD_NAME ) )
						->get('channel_fields')
						->result_array();


		foreach( $fields as $field )
		{
			$meta[ $field[ 'field_name'] ] = $field['field_id'];
		}

		return $meta;

	}

	public function get_data( $entry_id = 0 )
	{
		if( $entry_id == 0 ) return array();

		$meta = $this->get_meta();

		$data =ee()->db->select('field_id_'. $meta[SHORTLIST_UNIQUE_FIELD_NAME].' AS unqiue, field_id_'. $meta[SHORTLIST_DATABLOCK_FIELD_NAME].' AS datablock')
						->where('entry_id', $entry_id )
						->get('channel_data')
						->row_array();

		if( empty( $data ) ) return array();

		// Now unwrap the datablock
		$datablock = $data['datablock'] = unserialize( base64_decode( $data['datablock'] ) );

		if( !is_array( $datablock ) ) return array();

		$data['datablock'] = $datablock;

		return $data;
	}
	// --------------------------------------------------------------------

	private function _unique_field_id()
	{
		$row = ee()->db->select('field_id')
						->where('field_name', SHORTLIST_UNIQUE_FIELD_NAME )
						->get('channel_fields')
						->row_array();


		return $row['field_id'];
	}

	// --------------------------------------------------------------------
	private function _install_fieldgroup()
	{
		$data = array(
			'group_id'		=>	'',
			'site_id'		=>	ee()->config->item('site_id'),
			'group_name'	=>	SHORTLIST_GROUP_NAME
		);

		ee()->db->insert('field_groups', $data);

		$field_group_id = ee()->db->insert_id();

		return $field_group_id;
	}

	// --------------------------------------------------------------------

	private function _uninstall_fieldgroup()
	{
		ee()->db->where('group_name', SHORTLIST_GROUP_NAME)
						->delete('field_groups');
	}

	// --------------------------------------------------------------------

	private function _uninstall_channel()
	{
		$channels = ee()->api_channel_structure->get_channels();

		foreach( $channels->result_array() as $row )
		{
			if( $row['channel_name'] == SHORTLIST_CHANNEL_NAME )
			{
				ee()->api_channel_structure->delete_channel( $row['channel_id'], ee()->config->item('site_id') );
			}
		}
	}

	// --------------------------------------------------------------------

	private function _install_channel($field_group_id )
	{
		$data = array(
			'site_id' 			=> ee()->config->item('site_id'),
			'channel_title' 	=> SHORTLIST_CHANNEL_LABEL,
			'channel_name'  	=> SHORTLIST_CHANNEL_NAME,
			'field_group'		=> $field_group_id,
			'url_title_prefix'	=> 'shortlist_');

		$channel_id = ee()->api_channel_structure->create_channel($data);

		return $channel_id;
	}

	// --------------------------------------------------------------------

	private function _uninstall_fields()
	{
		// Get the field ids we need to remove
		$results = ee()->db->from('channel_fields')
						->where_in('field_name', array(SHORTLIST_DATABLOCK_FIELD_NAME, SHORTLIST_UNIQUE_FIELD_NAME))
						->get();

		ee()->load->library('api');
		ee()->api->instantiate('channel_fields');
		ee()->api_channel_fields->fetch_all_fieldtypes();

		$rel_ids = array();
		$deleted_fields = array();

		if ($results->num_rows() > 0)
		{
			foreach ($results->result_array() as $field)
			{
				ee()->api_channel_fields->setup_handler($field['field_type']);
				ee()->api_channel_fields->delete_datatype($field['field_id'], $field);

				$deleted_fields['field_ids'][] = $field['field_id'];
				$deleted_fields['group_id'] = (isset($field['group_id'])) ? $field['group_id'] : '';
				$deleted_fields['field_label'] = (isset($field['field_label'])) ? $field['field_label'] : '';


				$channels = ee()->db->select('channel_id')
								->from('channels')
								->where('site_id', ee()->config->item('site_id'))
								->where('field_group', $field['group_id'])
								->get();

				$channel_ids = array();

				if ($channels->num_rows() > 0)
				{
					foreach ($channels->result() as $row)
					{
						$channel_ids[] = $row->channel_id;
					}

					ee()->load->library('layout');
					ee()->layout->delete_layout_fields($field['field_id'], $channel_ids);
				}

			}

			// Make sure a deleted field is not assigned as the search excerpt
			ee()->db->where_in('search_excerpt', $deleted_fields['field_ids']);
			ee()->db->update('channels', array('search_excerpt' => NULL));

			// Remove from field formatting
			ee()->db->where_in('field_id', $deleted_fields['field_ids']);
			ee()->db->delete('field_formatting');

			//  Get rid of any stray relationship data
			if (count($rel_ids) > 0)
			{
				ee()->db->where_in('rel_id', $rel_ids);
				ee()->db->delete('relationships');
			}
		}


		foreach( $deleted_fields['field_ids'] as $field_id )
		{
			ee()->db->where('field_id', $field_id)
							->delete('channel_fields');
		}

	}


	// --------------------------------------------------------------------


	private function _install_fields( $field_group_id )
	{
		// We have two fields we need
		// 1. Unique Val
		// 2. Data Block
		$fields = array();

		$order_count = 0;

		// 1. Unique Val text field
		$fields[] = array(
				'field_id'				=>	'',
				'site_id'				=>	ee()->config->item('site_id'),
				'group_id'				=>	$field_group_id,
				'field_name'			=>	SHORTLIST_UNIQUE_FIELD_NAME,
				'field_label'			=>	'Unique Value',
				'field_instructions'	=>	'<strong>Auto-populated</strong> A unique value for this entry as defined when the item was added to a shortlist',
				'field_type'			=>	'text',
				'field_list_items'		=>	'',
				'field_pre_populate'	=>	'n',
				'field_pre_blog_id'		=>	'0',
				'field_pre_field_id'	=>	'0',
			/*	'field_related_to'		=>	'blog',
				'field_related_id'		=>	'0',
				'field_related_orderby'	=>	'date',
				'field_related_sort'	=>	'desc',
				'field_related_max'		=>	'0',*/
				'field_ta_rows'			=>	'3',
				'field_maxl'			=>	'255',
				'field_required'		=>	'y',
				'field_text_direction'	=>	'ltr',
				'field_search'			=>	'n',
				'field_is_hidden'		=>	'y',
				'field_fmt'				=>	'none',
				'field_show_fmt'		=>	'n',
				'field_order'			=>	++$order_count
			);

		// 1. Data block textarea field
		$fields[] = array(
				'field_id'				=>	'',
				'site_id'				=>	ee()->config->item('site_id'),
				'group_id'				=>	$field_group_id,
				'field_name'			=>	SHORTLIST_DATABLOCK_FIELD_NAME,
				'field_label'			=>	'Datablock',
				'field_instructions'	=>	'<strong>Auto-populated</strong> Complete data for this item in array form',
				'field_type'			=>	'textarea',
				'field_list_items'		=>	'',
				'field_pre_populate'	=>	'n',
				'field_pre_blog_id'		=>	'0',
				'field_pre_field_id'	=>	'0',
				/*'field_related_to'		=>	'blog',
				'field_related_id'		=>	'0',
				'field_related_orderby'	=>	'date',
				'field_related_sort'	=>	'desc',
				'field_related_max'		=>	'0',*/
				'field_ta_rows'			=>	'3',
				'field_maxl'			=>	'255',
				'field_required'		=>	'y',
				'field_text_direction'	=>	'ltr',
				'field_search'			=>	'n',
				'field_is_hidden'		=>	'y',
				'field_fmt'				=>	'none',
				'field_show_fmt'		=>	'n',
				'field_order'			=>	++$order_count
			);

		foreach( $fields as $field )
		{
			$this->_update_channel_fields( $field );
		}

	}


	// --------------------------------------------------------------------

	/**
	 * Update channel fields
	 *
	 * In EE1.x we had a method 'update_weblog_fields()' that would take a
	 * _POST array and create a new field
	 * In EE2.x the corresponding method was moved to the controller
	 * (Admin_content::field_update()) so we can't use it
	 * This is a recreation of what that does
	 */

	private function _update_channel_fields( $data )
	{
		$field_type = $data['field_type'];
		$group_id = $data['group_id'];

		ee()->load->library('api');
		ee()->api->instantiate('channel_fields');
		ee()->load->model('field_model');

		$native = array(
			'field_id', 'site_id', 'group_id',
			'field_name', 'field_label', 'field_instructions',
			'field_type', 'field_list_items', 'field_pre_populate',
			'field_pre_channel_id', 'field_pre_field_id',
		//	'field_related_id', 'field_related_orderby', 'field_related_sort', 'field_related_max',
			'field_ta_rows', 'field_maxl', 'field_required',
			'field_text_direction', 'field_search', 'field_is_hidden', 'field_fmt', 'field_show_fmt',
			'field_order', 'field_content_type'
		);

		// Get the field type settings
		ee()->api_channel_fields->fetch_all_fieldtypes();
		ee()->api_channel_fields->setup_handler($field_type);
		$ft_settings = ee()->api_channel_fields->apply('save_settings', array($data));

		// Default display options
		foreach(array('smileys', 'glossary', 'spellcheck', 'formatting_btns', 'file_selector', 'writemode') as $key)
		{
			$ft_settings['field_show_'.$key] = 'n';
		}

		// Now that they've had a chance to mess with the POST array,
		// grab post values for the native fields (and check namespaced fields)
		foreach($native as $key)
		{
			$native_settings[$key] = isset($data[$key]) ? $data[$key] : '';
		}

		$native_settings['field_content_type']	= 'any';
		$native_settings['field_settings']		= base64_encode(serialize($ft_settings));

		// Make us a field
		if ($data['field_order'] == 0 OR $data['field_order'] == '')
		{
			$query = ee()->db->query("SELECT count(*) AS count FROM exp_channel_fields WHERE group_id = '".ee()->db->escape_str($group_id)."'");
			$data['field_order'] = $query->row('count') + 1;
		}

		if ( ! $native_settings['field_ta_rows'])
		{
			$native_settings['field_ta_rows'] = 0;
		}

		// as its new, there will be no field id, unset it to prevent an empty string from attempting to pass
		unset($native_settings['field_id']);

		ee()->db->insert('channel_fields', $native_settings);

		$insert_id = ee()->db->insert_id();
		$native_settings['field_id'] = $insert_id;

		switch ($native_settings['field_content_type'])
		{
			case 'numeric':
				$type = 'FLOAT DEFAULT 0';
				break;
			case 'integer':
				$type = 'INT DEFAULT 0';
				break;
			default:
				$type = 'text';
		}

		ee()->db->query("ALTER TABLE exp_channel_data ADD COLUMN field_id_".$insert_id.' '.$type);
		ee()->db->query("ALTER TABLE exp_channel_data ADD COLUMN field_ft_".$insert_id." tinytext NULL");
		ee()->db->query("UPDATE exp_channel_data SET field_ft_".$insert_id." = '".ee()->db->escape_str($native_settings['field_fmt'])."'");

		foreach (array('none', 'br', 'xhtml') as $val)
		{
			ee()->db->query("INSERT INTO exp_field_formatting (field_id, field_fmt) VALUES ('$insert_id', '$val')");
		}

		$collapse = ($native_settings['field_is_hidden'] == 'y') ? 'true' : 'false';
		$buttons = ($ft_settings['field_show_formatting_btns'] == 'y') ? 'true' : 'false';

		$field_info['publish'][$insert_id] = array(
							'visible'		=> 'true',
							'collapse'		=> $collapse,
							'htmlbuttons'	=> $buttons,
							'width'			=> '100%'
		);

		$query = ee()->field_model->get_assigned_channels($group_id);

		if ($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{
				$channel_ids[] = $row->channel_id;
			}

			ee()->load->library('layout');
			ee()->layout->add_layout_fields($field_info, $channel_ids);
		}

		$_final_settings = array_merge($native_settings, $ft_settings);
		unset($_final_settings['field_settings']);

		ee()->api_channel_fields->set_settings($native_settings['field_id'], $_final_settings);
		ee()->api_channel_fields->setup_handler($native_settings['field_id']);
		ee()->api_channel_fields->apply('post_save_settings', array($data));

		ee()->functions->clear_caching('all', '', TRUE);
	}
	/* END update_channel_fields */



} // End class

/* End of file Shortlist_project_model.php */