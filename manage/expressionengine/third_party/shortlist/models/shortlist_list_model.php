<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Shortlist List Model class
 *
 * @package         shortlist_ee_addon
 * @version         2.5.3
 * @author          Joel Bradbury ~ <joel@squarebit.co.uk>
 * @link            http://squarebit.co.uk/shortlist
 * @copyright       Copyright (c) 2013, Joel
 */
class Shortlist_list_model extends Shortlist_model {

	private $lists = array();
	private $default_title = 'New List';
	private $start_end_bridge_char = '-';
	private $member_marker = '{shortlist_member_marker}';

	/**
	 * Constructor
	 *
	 * @access      public
	 * @return      void
	 */
	function __construct()
	{
		// Call parent constructor
		parent::__construct();

		// Initialize this model

		$this->initialize(
			'shortlist_list',
			'list_id_int',
			array(
				'site_id'       		=> 'int(4) unsigned NOT NULL',
				'member_id'				=> 'int(10) unsigned NOT NULL',
				'session_id'			=> 'varchar(100) NOT NULL',
				'list_id'				=> 'varchar(100) NOT NULL',
				'list_title'			=> 'varchar(100) NOT NULL',
				'list_name'				=> 'varchar(255) NOT NULL',
				'list_url_title'		=> 'varchar(255) NOT NULL',
				'list_url_title_start'	=> 'varchar(255) NOT NULL',
				'list_url_title_end'	=> 'varchar(255) NOT NULL',
				'list_attr'				=> 'text',
				'added_on'				=> 'int(10) unsigned NOT NULL',
				'last_change'			=> 'int(10) unsigned NOT NULL',
				'cloned_from'			=> 'varchar(100)',
				'default_list'			=> 'int(10) unsigned NOT NULL',
				'private_list'			=> 'int(10) unsigned NOT NULL',
				'list_description'		=> 'text')
		);


		// If this is being called from the installer, the list_table won't
		// actually be here. The installer just blindly calls our construct
		// with no neat way to filter their calls out, it's a massive pita,
		// but we'll work around it by checking if our db table is in place
		if( empty( $this->lists ) )
		{
			if( isset( ee()->db->data_cache['table_names'] ) )
			{
				// We might have a prefix setup,
				if( in_array( ee()->db->dbprefix.'shortlist_list' , ee()->db->data_cache['table_names'] ) )
				{
					$this->lists = $this->all();
				}
			}
		}




	}

	// --------------------------------------------------------------------

	/**
	 * Installs given table
	 *
	 * @access      public
	 * @return      void
	 */
	public function install()
	{
		// Call parent install
		parent::install();

		// Add indexes to table
		ee()->db->query("ALTER TABLE {$this->table()} ADD INDEX (`site_id`)");
		ee()->db->query("ALTER TABLE {$this->table()} ADD INDEX (`member_id`)");
		ee()->db->query("ALTER TABLE {$this->table()} ADD INDEX (`session_id`)");
	}


	// --------------------------------------------------------------

	public function get_lists( $orderby = 'list_id_int', $sort = 'asc' )
	{
		// Clean up sort data
		if( $sort != 'asc' AND $sort != 'desc' ) $sort = 'asc';

		if( $orderby == '' ) $orderby = 'list_id_int';
		elseif( !$this->is_attribute($orderby) ) $orderby = 'list_id_int';
		ee()->db->order_by( $orderby, $sort );

		$this->lists = $this->all($orderby.'-'.$sort);
		if( empty( $this->lists ) ) return array();

		ee()->shortlist_core_model->flush_db();

		// Also populate with items
		$found_default = FALSE;
		$unset_as_default = array();

		$first_id = '';
		$first_key = '';

		foreach($this->lists as $key => $list)
		{
			if( $first_id == '')
			{
				$first_key = $key;
				$first_id = $list['list_id_int'];
			}
			$tmp = $list;
			$tmp['is_default'] = FALSE;

			if($tmp['default_list'] == '1' AND !$found_default)
			{
				$tmp['is_default'] = TRUE;
				$found_default = TRUE;
			}
			elseif( $tmp['default_list'] AND $found_default )
			{
				// We have duplicate default lists,
				// needs a cleanup
				$unset_as_default[] = $tmp['list_id_int'];
			}

			$this->lists[ $key ] = $tmp;
		}

		if(!$found_default)
		{
			// Make the first list the default

			$this->lists[ $first_key ]['is_default'] = TRUE;
			// Set this as default
			$this->_make_default( $first_id );
		}

		if( !empty( $unset_as_default ) ) $this->_unset_defaults( $unset_as_default );

		return $this->lists;
	}

	// --------------------------------------------------------------

	public function create_unless_exists($keys, $make_default, $matcher = 'list_title')
	{
		$this->lists = $this->all();
		foreach($this->lists as $list)
		{
			if($list[$matcher] == $keys[$matcher]) return;
		}

		return $this->create($keys, $make_default);
	}

	// --------------------------------------------------------------

	public function create( $keys = array(), $make_default = TRUE, $list_id = '', $sess = array() )
	{
		//if( empty( $keys ) ) return FALSE;

		// Validate the keys
		// We might actually have no keys at all, and still be valid
		$vars = array();

		// Pull all the current lists for this user
		$this->lists = $this->all();

		$vars['item_title'] = $this->_generate_title( $keys );

		$vars['list_name'] = '';
		if(isset($keys['list_name']) AND $keys['list_name']!='') $vars['list_name'] = $keys['list_name'];

		$tmp_url_title = '';
		if( isset( $keys['list_url_title'] ) != '' ) $tmp_url_title = $keys['list_url_title'];
		if( !isset( $keys['list_url_title_start'] ) ) $keys['list_url_title_start'] = '';
		if( !isset( $keys['list_url_title_end'] ) ) $keys['list_url_title_end'] = '';

		foreach($keys as $key => $val)
		{
			$keys[$key] = $this->parse_member_marker($val);
		}

		if( $list_id == '' ) $list_id = random_string( 'alnum', 16 );

		if( empty( $sess ) ) $sess = ee()->shortlist_core_model->get_session();

		$word_separator = ee()->config->item('word_separator');
		ee()->load->helper('url');

		// Setup the bare bones
		$data['site_id'] 		= ee()->config->item('site_id');
		$data['member_id'] 		= '0';
		$data['session_id'] 	= '';
		$data['list_id'] 		= $list_id;
		$data['list_title']		= $vars['item_title'];

		$data['list_url_title']		= $this->_make_url_title( $tmp_url_title, $vars['item_title'] );

		// Only try and make url_titles for _start and _end if we need to
		if( $keys['list_url_title_start'] != '' )
		{
			$data['list_url_title_start'] = url_title($keys['list_url_title_start'], $word_separator, TRUE);
		}

		if( $keys['list_url_title_end'] != '' )
		{
			$data['list_url_title_end'] = $this->_make_url_title_end( $keys['list_url_title_end'], (isset($data['list_url_title_start']) ? $data['list_url_title_start'] : ''));
		}


		$data['added_on']		= ee()->localize->now;
		$data['last_change']	= ee()->localize->now;
		$data['cloned_from']	= '';
		$data['default_list'] 	= 0;
		$data['list_name'] 		= $vars['list_name'];

		if( $make_default ) $data['default_list'] = 1;

		$data[$sess['key']]		= $sess['val'];

		$r = self::insert( $data );

		// Empty the persistent lists array so it'll be refreshed with this new list
		$this->lists = array();
		if( $make_default ) $this->_make_default( $r );

		return $data;
	}

	public function get( $list_id )
	{
		// Limited to just the current user
		return $this->_get_list_by_id_for_user( $list_id );
	}


	public function get_list( $list_id )
	{
		// Have this cached?
		if(!isset(ee()->session->cache['shortlist']['list_id'][$list_id]))
		{
			// Expressly not limiting this to the current user
			$l = self::get_one($list_id, 'list_id');

			// Fallback to the list_url_title just in case
			if( empty($l) ) $l = self::get_one( $list_id, 'list_url_title');

			if(isset( $l['default_list'] ) AND $l['default_list'] == '1')
			{
				$l['is_default'] = TRUE;
			}

			ee()->session->cache['shortlist']['list_id'][$list_id] = $l;
		}

		ee()->shortlist_core_model->flush_db();

		return ee()->session->cache['shortlist']['list_id'][$list_id];
	}


	public function get_list_by_url( $list_url_title )
	{
		// Expressly not limiting this to the current user
		$l = self::get_one( $list_url_title, 'list_url_title');

		if(isset( $l['default_list'] ) AND $l['default_list'] == '1')
		{
			$l['is_default'] = TRUE;
		}

		return $l;
	}

	public function get_list_by_name( $list_name )
	{
		// Limiting to the current user
		ee()->shortlist_core_model->filter_by_current_user();
		$l = self::get_one( $list_name, 'list_name');

		if(isset( $l['default_list'] ) AND $l['default_list'] == '1')
		{
			$l['is_default'] = TRUE;
		}

		return $l;
	}



	public function get_lists_by_start_end( $list_url_title_start = '', $list_url_title_end = '', $orderby = 'list_id_int', $sort = 'asc' )
	{
		// Clean up sort data
		if( $sort != 'asc' AND $sort != 'desc' ) $sort = 'asc';

		if( $orderby == '' ) $orderby = 'list_id_int';
		elseif( !$this->is_attribute($orderby) ) $orderby = 'list_id_int';

		ee()->db->order_by( $orderby, $sort );

		// Expressly not limiting this to the current user
		if( $list_url_title_start == '' AND $list_url_title_end == '' ) return array();

		if( $list_url_title_start != '' ) ee()->db->where('list_url_title_start', ee()->security->xss_clean($list_url_title_start) );

		if( $list_url_title_end != '' ) ee()->db->where('list_url_title_end', ee()->security->xss_clean($list_url_title_end) );

		$l = self::get_all();
		if( empty( $l ) ) return array();

		// Also populate with items
		$found_default = FALSE;

		$first_id = '';
		$first_key = '';


		foreach($l as $key => $list)
		{
			if( $first_id == '')
			{
				$first_key = $key;
				$first_id = $list['list_id_int'];
			}
			$tmp = $list;
			$tmp['is_default'] = FALSE;

			if($tmp['default_list'] == '1' AND !$found_default)
			{
				$tmp['is_default'] = TRUE;
				$found_default = TRUE;
			}

			$l[ $key ] = $tmp;
		}

		if(!$found_default)
		{
			// Make the first list the default

			$l[ $first_key ]['is_default'] = TRUE;
			// Set this as default
			$this->_make_default( $first_id );
		}

		return $l;
	}

	// --------------------------------------------------------------

	public function get_create_list( $data = array() )
	{
		// If we're passed a list_id, and the current user has access
		// we'll return that list. Otherwise we'll try and pull the
		// default list for this user.
		// Lastly if no lists exist, we'll create a new one and
		// return that

		$list_id = '';
		if( isset( $data['list_id'] ) ) $list_id = $data['list_id'];

		$list = array();

		// Is this list_id in the user's possible lists?
		$list = $this->_get_list_by_id_for_user( $list_id );

		if( !$list )
		{
			// Get the default
			$list = $this->_get_default_list_for_user();
		}

		if( empty($list) OR $list === FALSE )
		{
			// Create a new list (and make it default)
			$list = $this->create( array(), TRUE );
		}

		return $list;
	}

	// --------------------------------------------------------------

	public function get_default_list()
	{
		return $this->_get_default_list_for_user();
	}

	// --------------------------------------------------------------

	public function update_default( $keys = array() )
	{
		if( empty( $keys ) ) return FALSE;

		$list_id = '0';
		if( isset( $keys['list_id'] ) ) $list_id = $keys['list_id'];
		if( $list_id == '0' ) return FALSE;

		// Pass this over to the _make_default() method
		// That also validates that the user has permission to actually
		// set this as default

		$state = $this->_make_default( $list_id, 'key' );

		return $state;
	}

	// --------------------------------------------------------------

	public function clear_list( $keys = array() )
	{
		if( empty( $keys ) ) return FALSE;

		if(isset($keys['all_lists']) AND $keys['all_lists']==TRUE)
		{
			// Clear all the the lists
			foreach($this->all() as $list)
			{
				$this->clear_list(array('list_id'=>$list['list_id']));
			}
		}

		$list_id = '0';
		if( isset( $keys['list_id'] ) ) $list_id = $keys['list_id'];
		if( $list_id == '0' ) return FALSE;

		// Validate this user has permission for this
		$list = $this->get_list( $list_id );
		if( $list == FALSE ) return FALSE;

		// Looks ok. Clear it
		self::update( $list['list_id_int'], array('last_change'=>ee()->localize->now) );

		// Also remove the child items
		ee()->shortlist_item_model->delete( $list['list_id'], 'list_id' );

		return $list['list_id'];
	}

// --------------------------------------------------------------

	public function remove_list( $keys = array() )
	{
		if( empty( $keys ) ) return FALSE;

		$list_id = '0';
		if( isset( $keys['list_id'] ) ) $list_id = $keys['list_id'];
		if( $list_id == '0' ) return FALSE;

		// Validate this user has permission for this
		$list = $this->get_list( $list_id );
		if( $list == FALSE ) return FALSE;

		// Looks ok. Clear it
		self::delete( $list['list_id_int'] );

		// Also remove the child items
		ee()->shortlist_item_model->delete( $list['list_id'], 'list_id' );

		return $list['list_id'];
	}



	// --------------------------------------------------------------

	public function edit_list( $list )
	{

		// Currently we're only allowing users to change two data pieces
		// for a list. Later we might allow more, but lets just grab
		// these bits to start with
		$list_title = ee()->security->xss_clean( ee()->input->get_post('list_title') );
		$list_url_title = ee()->security->xss_clean( ee()->input->get_post('list_url_title') );

		$list_url_title_start = ee()->security->xss_clean( ee()->input->get_post('list_url_title_start') );
		$list_url_title_end = ee()->security->xss_clean( ee()->input->get_post('list_url_title_end') );

		$private_list = ee()->security->xss_clean( ee()->input->get_post('private_list') );
		$list_description = ee()->security->xss_clean( ee()->input->get_post('list_description') );


		// If all are blank bail
		if( trim($list_title) == '' AND trim( $list_url_title ) == '' AND trim($list_url_title_start) == '' AND trim($list_url_title_end) == '' AND trim($private_list) == '') return FALSE;

		$data = array();

		// Ok, there looks like there might be something to do. Deal with list_title first
		if( trim($list_title) != '' )
		{
			$data['list_title'] = trim( $list_title );
		}


		// If we have a _start and _end supplied, use store those, and use that to generate a url_title too
		// both must be passed for this to be valid
		if( trim($list_url_title_start) != '' OR trim( $list_url_title_end ) != '' )
		{
			$word_separator = ee()->config->item('word_separator');
			ee()->load->helper('url');


			// Parse out our marker vars
			$list_url_title_start = $this->parse_member_marker($list_url_title_start);
			$list_url_title_end = $this->parse_member_marker($list_url_title_end);

			// We allow duplicates on both the _start and _end values, but the url_title
			// still keeps uniqueness
			$temp_url_title = '';
			if( $list_url_title_start != '' ) $temp_url_title .= strtolower( $list_url_title_start );

			if( $list_url_title_start != '' AND $list_url_title_end != '' ) $temp_url_title .= $this->start_end_bridge_char;

			if( $list_url_title_end != '' ) $temp_url_title .= strtolower( $list_url_title_end );

			$data['list_url_title'] = $this->_make_url_title( strtolower($temp_url_title), $list['list_id'] );

			if( $list_url_title_start != '' )
			{
				$list_url_title_start = url_title( strtolower($list_url_title_start), $word_separator, TRUE);
			}

			$data['list_url_title_end'] = $this->_make_url_title_end( strtolower($list_url_title_end), strtolower($list_url_title_start),  $list['list_id']);

			if( $list_url_title_start != '' ) $data['list_url_title_start'] = $list_url_title_start;

		}
		else
		{
			// Fall back to standard url_title handling
			if( trim($list_url_title) != '' AND $list_url_title != $list['list_url_title'])
			{

				$list_url_title = $this->parse_member_marker($list_url_title);

				// url titles are tricker.
				// They need to be unique and also have a limited character set to work from
				// Make a url title from this if we can
				$data['list_url_title'] = strtolower( $this->_make_url_title( $list_url_title, $list['list_id'] ) );
			}
			else if( trim($list_url_title) == '' AND $list['list_url_title'] == '')
			{
				// None passed, generate one from the available title
				$temp_title = $list['list_title'];
				if( isset( $data['list_title'] ) ) $temp_title = $data['list_title'];
				$data['list_url_title'] = strtolower( $this->_make_url_title( $temp_title, $list['list_id'] ) );
			}
		}


		// Now handle the public/private list options
		if( trim($private_list) != '')
		{
			$pri_state = '';

			// We'll play nice and let them be a bit loose and wild with their data
			if( trim($private_list) == 'private')
			{
				$pri_state = 'private';
			}
			elseif( trim($private_list) == 'public')
			{
				$pri_state = 'public';
			}
			else
			{
				$is_yes = ee()->shortlist_core_model->check_yes_no('yes', trim($private_list));
				$is_no = ee()->shortlist_core_model->check_yes_no('no', trim($private_list));

				if( $is_yes ) $pri_state = 'private';
				elseif( $is_no ) $pri_state = 'public';
			}

			if($pri_state != '')
			{
				if($pri_state == 'private') $data['private_list'] = '1';
				else $data['private_list'] = '';
			}
		}



		// Now handle the list description
		$data['list_description'] = $list_description;



		if( empty( $data ) ) return FALSE;


		// Update the list
		self::update( $list['list_id_int'], $data );
		return $data;
	}


	// --------------------------------------------------------------

	private function _make_url_title( $raw = '', $title = '', $list_id = 0 )
	{
		$url_title = ee()->shortlist_core_model->validate_url_title( $raw, $title,  $list_id );

		return $url_title;
	}

	// --------------------------------------------------------------

	private function _make_url_title_end( $raw = '', $list_url_title_start = '', $list_id = FALSE )
	{
		if( $list_url_title_start == '' AND $list_id !== FALSE )
		{
			// We haven't got a url_title_start passed, but we need it to carry on
			ee()->db->select( array('list_url_title_start'));
			$list_start = self::get_one( $list_id, 'list_id');

			$list_url_title_start = $list_start['list_url_title_start'];
		}

		$url_title = ee()->shortlist_core_model->validate_url_title_end( $raw, $list_url_title_start, $list_id );

		return $url_title;
	}

	// --------------------------------------------------------------

	private function _make_default( $default_id = '', $type = 'int' )
	{
		if( $default_id == '' ) return FALSE;
		if( empty($this->lists) ) $this->lists = $this->all();

		// Is the key valid for this set?
		$ids = array();
		$new_default_id = 0;

		if( $type == 'int' ) $new_default_id = $default_id;


		foreach( $this->lists as $key => $list )
		{
			$ids[] = $list['list_id_int'];

			if( $type == 'key' AND $list['list_id'] == $default_id ) $new_default_id = $list['list_id_int'];
		}

		if( !in_array( $new_default_id, $ids ) ) return FALSE;

		// Ok, set it, and also unset everything else
		$this->_unset_defaults( $ids );
		parent::update( $new_default_id, array('default_list' => '1') );

		return TRUE;
	}

	// --------------------------------------------------------------

	/*
	* Tests is a supplied list_id is in the current
	* user's available list array.
	* Returns the list if present, false if not
	*/
	private function _get_list_by_id_for_user( $list_id )
	{
		if( empty( $this->lists ) ) {
			$this->lists = $this->all();
			if(empty($this->lists)) return FALSE;
		}

		foreach( $this->lists as $list )
		{
			if( $list['list_id'] == $list_id ) return $list;
		}

		return FALSE;
	}

	// --------------------------------------------------------------

	/*
	* Gets the default list for the current user
	* If no available lists, returns false
	*/
	private function _get_default_list_for_user()
	{
		if( empty( $this->lists ) ) return FALSE;

		foreach( $this->lists as $list )
		{
			if( $list['default_list'] ) return $list;
		}

		return FALSE;
	}

	// --------------------------------------------------------------

	private function _unset_defaults( $ids )
	{
		// Build the query
		ee()->db->where_in('list_id_int', $ids );
		parent::update_group( array('default_list' => '0') );
	}


	// --------------------------------------------------------------

	private function _generate_title( $keys = array() )
	{
		$passed = ( isset( $keys['list_title'] ) ? $keys['list_title'] : '' );

		// Is there already a list of this name by the user?
		// pull the entire current list set for this user
		if( $passed == '' ) $passed = $this->default_title;
		if( empty( $this->lists ) ) return $passed;

		// Otherwise there might be a name conflict, check that
		$titles = array();
		foreach( $this->lists as $list )
		{
			$titles[] = $list['list_title'];
		}

		if( in_array( $passed, $titles ) ) $passed = $this->_make_new_title( $passed, $titles );

		return $passed;
	}

	// --------------------------------------------------------------

	private function _make_new_title( $title, $list )
	{
		$found = false;
		$i = 0;
		$base = $title;

		while( !$found )
		{
			if( in_array($title, $list) )
			{
				$i++;
				$title = $base .' ('.$i.')';
			}
			else $found = true;
		}

		return $title;
	}

	// --------------------------------------------------------------

	public function all($key = 'default')
	{
		if(!isset(ee()->session->cache['shortlist']['lists'][$key]))
		{
			ee()->shortlist_core_model->filter_by_current_user();
			ee()->session->cache['shortlist']['lists'][$key] = self::get_all();
		}

		ee()->shortlist_core_model->flush_db();

		return ee()->session->cache['shortlist']['lists'][$key];
	}


	// --------------------------------------------------------------

	public function filter_by_default_list()
	{
		$list = $this->_get_default_list_for_user();
		ee()->db->where('list_id', $list['list_id'] );
	}

	// --------------------------------------------------------------

	public function reassign_to_member( $member_id, $session_id, $merge = FALSE )
	{
		$member_lists = array();

		if( $merge == TRUE )
		{
			// Get lists
			$member_lists = self::get_some( $member_id, 'member_id');
		}


		if( !empty( $member_lists ) )
		{
			// Just kill this list(s)
			self::delete( $session_id, 'session_id');
		}
		else
		{
			$data['member_id'] = $member_id;
			$data['session_id'] = '';

			ee()->db->where('session_id', $session_id );
			self::update_group( $data );
		}

		return TRUE;
	}

	// --------------------------------------------------------------

	public function parse_member_marker( $str = array() )
	{
		if( $str == '' ) return '';

		// We let devs pass a marker in place of their user setup
		// for flexbility when dealing with guests, and to jump around
		// parse order issues
		if( strpos($str, $this->member_marker) > -1 )
		{
			// Figure out what to replace it with

			$sess = ee()->shortlist_core_model->get_session();
			if( $sess['key'] == 'member_id')
			{
				// Member
				$replace = ee()->session->userdata['member_id'];
			}
			else
			{
				// Guest
				$replace = 'guest-'.$sess['val'];
			}

			$str = str_replace($this->member_marker, $replace, $str);
		}

		return $str;
	}





} // End class

/* End of file Shortlist_project_model.php */