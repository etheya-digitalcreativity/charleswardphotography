<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Shortlist Item Model class
 *
 * @package         shortlist_ee_addon
 * @version         2.5.3
 * @author          Joel Bradbury ~ <joel@squarebit.co.uk>
 * @link            http://squarebit.co.uk/shortlist
 * @copyright       Copyright (c) 2013, Joel
 */
class Shortlist_item_model extends Shortlist_model {


	private $lists = array();

	// --------------------------------------------------------------------
	// METHODS
	// --------------------------------------------------------------------

	/**
	 * Constructor
	 *
	 * @access      public
	 * @return      void
	 */
	function __construct()
	{
		// Call parent constructor
		parent::__construct();

		// Initialize this model
		$this->initialize(
			'shortlist_item',
			'item_id',
			array(
				'site_id'       		=> 'int(4) unsigned NOT NULL',
				'member_id'				=> 'int(10) unsigned NOT NULL',
				'session_id'			=> 'varchar(100) NOT NULL',
				'entry_id'				=> 'int(10) unsigned',
				'unique_val'			=> 'varchar(100)',
				'added'					=> 'int(10) unsigned NOT NULL',
				'item_order'			=> 'int(4) unsigned NOT NULL',
				'list_id'				=> 'varchar(100) NOT NULL',
				'cloned_from'			=> 'varchar(100)')
		);

	}

	// --------------------------------------------------------------------

	/**
	 * Installs given table
	 *
	 * @access      public
	 * @return      void
	 */
	public function install()
	{
		// Call parent install
		parent::install();

		// Add indexes to table
		ee()->db->query("ALTER TABLE {$this->table()} ADD INDEX (`site_id`)");
		ee()->db->query("ALTER TABLE {$this->table()} ADD INDEX (`member_id`)");
		ee()->db->query("ALTER TABLE {$this->table()} ADD INDEX (`session_id`)");
	}




	public function get_list( $list_id = '', $join = FALSE, $get_list = FALSE )
	{
		if( $list_id == '' ) return array();

		if( $join == TRUE )
		{
			ee()->db->select('shortlist_item.*, channel_titles.title')
						->join('channel_titles', 'shortlist_item.entry_id = channel_titles.entry_id');
		}

		ee()->db->order_by( 'item_order', 'asc');

		$items = self::get_some( $list_id, 'list_id' );

		if( $get_list == TRUE )
		{
			$list = ee()->shortlist_list_model->get_list( $list_id );
			$list = $this->prefix( $list, 'list');

			// Merge the items
			foreach( $items as $key => $item )
			{
				$items[$key] = array_merge( $item, $list );
			}
		}

		return $items;
	}

	// --------------------------------------------------------------

	public function get_entry_id_from_item_id( $id )
	{
		$attr = 'item_id';
		$ret = parent::get_one( $id, $attr );

		if( empty( $ret ) ) return FALSE;

		return $ret['entry_id'];
	}

	public function get_one_by_entry_id( $entry_id )
	{
		ee()->db->select('shortlist_item.*, channel_titles.title')
						->join('channel_titles', 'shortlist_item.entry_id = channel_titles.entry_id');

		return self::get_some( $entry_id, 'shortlist_item.entry_id' );
	}
	// --------------------------------------------------------------

	public function get_one($id, $attr = FALSE)
	{
		ee()->shortlist_core_model->filter_by_current_user();
		if( $attr == FALSE ) $attr = 'item_id';
		$ret = self::get_some( $id, $attr );

		if( empty( $ret ) ) return array();

		$row = current( $ret );
		return $row;
	}

	// --------------------------------------------------------------

	public function get_list_single( $list_id )
	{

		ee()->db->select('*, COUNT(*) as c')
						->order_by( 'added', 'desc' )
						->where('list_id', $list_id );

		$items = parent::get_all();

		return $items;
	}


	// --------------------------------------------------------------

	public function get_all_items()
	{
		ee()->db->select('shortlist_item.*, channel_titles.title, COUNT(*) as c')
						->group_by('shortlist_item.entry_id')
						->join('channel_titles', 'shortlist_item.entry_id = channel_titles.entry_id')
						->order_by( 'c', 'desc' );

		$items = parent::get_all();

		return $items;
	}


	public function get_lists_with_entry( $entry_id )
	{
		ee()->db->select( '*')
						->where( 'entry_id', $entry_id );
		$lists = parent::get_all();
		// We also need to count how many total entries each of these lists has

		$list_ids = array();
		foreach( $lists as $key => $list )
		{
			$list_ids[] = $list['list_id'];
		}

		$list_more = $this->get_some_lists( $list_ids );

		return $list_more;
	}

	public function get_some_lists( $list_ids = array() )
	{
		if( empty( $list_ids ) ) return array();

		ee()->db->select('*, COUNT(*) as c')
						->group_by( 'list_id' )
						->order_by( 'added', 'desc' )
						->where_in('list_id', $list_ids);

		$items = parent::get_all();

		return $items;
	}
	// --------------------------------------------------------------

	public function get_all_lists()
	{

		ee()->db->select('*, COUNT(*) as c')
						->group_by( 'list_id' )
						->order_by( 'added', 'desc' );

		$items = parent::get_all();

		return $items;
	}

	// --------------------------------------------------------------

	public function get_all( $join = FALSE )
	{
		if( $join )
		{
			ee()->db->join('channel_titles', 'shortlist_item.entry_id = channel_titles.entry_id');
		}

 		$sess = ee()->shortlist_core_model->get_session();
		//		ee()->db->order_by( 'item_order', 'asc');

		return self::get_some( $sess['val'], $sess['key'] );

	}



	// --------------------------------------------------------------


	public function get_clones( $list_id = '' )
	{
		if( $list_id == '' ) return array();

		ee()->db->select('shortlist_item.*, channel_titles.title')
						->join('channel_titles', 'shortlist_item.entry_id = channel_titles.entry_id')
						->where('cloned_from', $list_id)
						->order_by('added', 'desc');

		return parent::get_all();
	}
	// --------------------------------------------------------------



	public function get_item( $entry_id = '', $list_id = '')
	{
		// If we've been passed an entry_id on the params use it
		if( $entry_id == '' ) $entry_id = ee()->TMPL->fetch_param('entry_id');

		if( $entry_id != '' )
		{
			if(!isset(ee()->session->cache['shortlist']['item_entry_list'][$list_id.'|'.$entry_id]) )
			{
				if( $list_id != '' ) ee()->db->where('list_id', $list_id);
				$item = $this->get_one( $entry_id, 'entry_id' );

				ee()->session->cache['shortlist']['item_entry_list'][$list_id.'|'.$entry_id] = $item;
			}

			$item = ee()->session->cache['shortlist']['item_entry_list'][$list_id.'|'.$entry_id];

			if( empty( $item ) ) return array();

			$item['in_list'] = TRUE;

			return $item;
		}

		// Otherwise, use the passed params to search for the item
		$is_external = ee()->TMPL->fetch_param('is_external');

		if( $is_external != 'yes' ) return array();

		// Do we have a marker for unique attribute?
		$unique_attr = ee()->TMPL->fetch_param('unique_attr');
		if( $unique_attr == '' ) return array();


		$unique_val = ee()->TMPL->fetch_param( 'attr:'.$unique_attr );
		if( $unique_val == '' )
		{
			// Fallback to the attr w/out attr: prepended just in case
			$unique_val = ee()->TMPL->fetch_param( $unique_attr );
			if( $unique_val == '' ) return array();
		}

		// Now look in the db for this attribute
		if( $list_id != '' ) ee()->db->where('list_id', $list_id);
		$item = $this->get_one( $unique_val, 'unique_val' );
		if( empty( $item ) ) return array();

		$item['in_list'] = TRUE;
		return $item;
	}


	// --------------------------------------------------------------

	public function clone_list( $keys = array() )
	{
		if( empty( $keys ) OR !isset( $keys['list_id'] ) ) return FALSE;

		// Get the list items
		$items = $this->get_list( $keys['list_id'] );

		// Now add each item one-by-one, with a marker of where they came from
		$new_keys['cloned_from'] = $keys['list_id'];


		foreach( $items as $item )
		{
			$new_keys['entry_id'] = $item['entry_id'];

			if( $item['unique_val'] != '' )
			{
				$new_keys['clone_item'] = TRUE;
				$new_keys['unique_val'] = $item['unique_val'];
			}

			$this->add( $new_keys );
		}

		return;
	}


	// --------------------------------------------------------------

	public function add( $keys = array(), $member_id_override = '' )
	{
		if( empty( $keys ) ) return FALSE;

		if( isset( $keys['clone_item'] ) )
		{
			$data['unique_val'] = $keys['unique_val'];
			$entry_id = $keys['entry_id'];
		}
		elseif( isset( $keys['entry_id'] ) )
		{
			// This is a local entry
			$entry_id = $keys['entry_id'];
		}
		elseif( isset( $keys['item_id'] ) )
		{
			// This is coming from an existing list item
			// Get the entry id from this
			$entry_id = $this->get_entry_id_from_item_id( $keys['item_id'] );

			if( $entry_id == FALSE ) return FALSE;
		}
		else
		{
			// This is an external item,
			// we'll have to either create it, or
			// find it's dummy ee entry first
			$entry_id = $this->_find_create_entry( $keys );

			// Do we have a marker for unique attribute?
			if( !isset( $keys['unique_attr'] ) ) return FALSE;
			$unique_attr = $keys['unique_attr'];
			if( $unique_attr == '' ) return FALSE;

			if( !isset( $keys['attr:'. $unique_attr ] ) )
			{
				if( !isset( $keys[ $unique_attr ] ) ) return FALSE;
				else $unique_val = $keys[ $unique_attr ];
			}
			else
			{
				$unique_val = $keys[ 'attr:' . $unique_attr ];
			}

			$data['unique_val'] = $unique_val;


			if( $entry_id === FALSE ) return FALSE;

		}
		ee()->load->helper('string');

		// Just handle adding ee entries for the moment
		$data['site_id'] 		= ee()->config->item('site_id');
		$data['member_id'] 		= ee()->session->userdata('member_id');
		$data['session_id'] 	= ee()->shortlist_core_model->get_create_session();
		$data['entry_id'] 		= $entry_id;
		$data['added'] 			= ee()->localize->now;
		$data['item_order']		= 0;
		$data['list_id'] 		= random_string( 'alnum', 16 );

		// Let the member_id get overridden for imports
		if( $member_id_override != '') $data['member_id'] = $member_id_override;

		$cloned_from = '';
		if( isset( $keys['cloned_from'] ) AND $keys['cloned_from'] != '' ) $cloned_from = $keys['cloned_from'];
		$data['cloned_from'] = $cloned_from;

		// Do we have a list id passed on the request?
		if( isset( $keys['list_id'] ) AND $keys['list_id'] != '' ) $data['list_id'] = $keys['list_id'];
		elseif( isset($keys['list_url_title']) OR isset( $keys['list_url_title_start'] ) OR isset($keys['list_url_title_end'] ) OR isset($keys['list_name']) ) {

			$list_id = $this->extract_list_id( $keys );

			if($list_id != '') $data['list_id'] = $list_id;
		}


		// Do we have a remove marker?
		if( isset($keys['remove_from_list_id']) OR isset($keys['remove_from_list_url_title']) OR isset($keys['remove_from_list_url_title_start']) OR isset($keys['remove_from_list_url_title_end']) OR isset($keys['remove_from_list_name']) )
		{
			$remove_keys = array();

			$remove_keys['entry_id'] = $entry_id;

			if(isset($keys['remove_from_list_id'])) $remove_keys['list_id'] = $keys['remove_from_list_id'];
			else $remove_keys['list_id'] = $this->extract_list_id($keys, 'remove_from_');

			$this->remove($remove_keys);

		}

		// Check we've not already got a record for this item
		if( $item_id = $this->_exists_by_id( $entry_id, $data['list_id'] ) !== FALSE ) return $item_id;

		// Does this user already have any list items?
		$current = array();
		if($member_id_override == '') $current = $this->_get_current( $data );

		if( isset( $current['list_id'] ) ) $data['list_id'] = $current['list_id'];
		if( isset( $current['next_order'] ) ) $data['item_order'] = $current['next_order'];


		$item_id = self::insert( $data );


		return $item_id;
	}

	// --------------------------------------------------------------

	public function clear()
	{
		ee()->shortlist_core_model->filter_by_current_user();
		self::delete();

		return TRUE;
	}

	// --------------------------------------------------------------

	public function remove( $keys = array() )
	{
		if( empty( $keys ) ) return FALSE;

		if( isset( $keys['item_id'] ) )
		{
			// Directly remove this item
			self::delete( $keys['item_id'] );
			return TRUE;
		}
		elseif( isset( $keys['entry_id'] ) )
		{
			$entry_id = $keys['entry_id'];

			$list_id = '';
			if( isset( $keys['list_id'] ) ) $list_id = $keys['list_id'];

			$item_id = $this->_exists_by_id( $entry_id, $list_id );

			// Check we've not already got a record for this item
			if( $item_id === FALSE ) return TRUE;

			self::delete( $item_id );
			return TRUE;
		}
		elseif( isset( $keys['is_external'] ) AND $keys['is_external'] == 'yes' )
		{

			// Do we have a marker for unique attribute?
			if( !isset( $keys['unique_attr'] ) ) return FALSE;
			$unique_attr = $keys['unique_attr'];
			if( $unique_attr == '' ) return FALSE;

			if( !isset( $keys['attr:'. $unique_attr ] ) )
			{
				if( !isset( $keys[ $unique_attr ] ) ) return FALSE;
				else $unique_val = $keys[ $unique_attr ];
			}
			else
			{
				$unique_val = $keys[ 'attr:' . $unique_attr ];
			}

			if( $unique_val == '' ) return FALSE;

			$item_id = $this->_exists_by_unique( $unique_val );

			// Check we've not already got a record for this item
			if( $item_id === FALSE ) return TRUE;

			self::delete( $item_id );
			return TRUE;
		}

		return FALSE;
	}


	// --------------------------------------------------------------

	private function _get_current( $data = array() )
	{
		if( empty( $data ) ) return array();


		// Get the current default list, or create a new one if none exists
		$this_list = ee()->shortlist_list_model->get_create_list( $data );



		// Look for existing items for this user
		ee()->db->select( array('list_id', 'item_order') );

		// Filter by current user
		ee()->shortlist_core_model->filter_by_current_user();

		ee()->db->where('list_id', $this_list['list_id'])
						->order_by('item_order', 'desc')
						->limit(1);


		$result = self::get_all();
		$next_order = 0;
		if( !empty( $result ) )
		{
			$row = current( $result );
			$next_order = $row['item_order'] + 1;
		}


		return array('list_id' => $this_list['list_id'], 'next_order' => $next_order);

	}

	// --------------------------------------------------------------

	private function _exists_by_unique( $unique_val )
	{
		ee()->db->select('item_id');
		ee()->shortlist_core_model->filter_by_current_user();

		ee()->db->where('unique_val', $unique_val );
		$result = self::get_all();

		if( empty( $result ) ) return FALSE;
		$row = current( $result );

		return $row['item_id'];
	}




	// --------------------------------------------------------------

	private function _exists_by_id( $entry_id, $list_id = '' )
	{
		ee()->db->select('item_id');
		ee()->shortlist_core_model->filter_by_current_user();

		if( $list_id != '' ) ee()->db->where('list_id', $list_id );
		else ee()->shortlist_list_model->filter_by_default_list();

		ee()->db->where('entry_id', $entry_id );

		$result = self::get_all();

		if( empty( $result ) ) return FALSE;
		$row = current( $result );

		return $row['item_id'];
	}

	// --------------------------------------------------------------

	private function _find_create_entry( $keys = array() )
	{
		if( empty( $keys ) ) return FALSE;

		// Check we're looking for an external item
		if( !(isset( $keys['is_external'] ) AND $keys['is_external'] == 'yes') ) return FALSE;

		// Do we have a marker for unique attribute?
		if( !isset( $keys['unique_attr'] ) ) return FALSE;
		$unique_attr = $keys['unique_attr'];
		if( $unique_attr == '' ) return FALSE;

		if( !isset( $keys['attr:'. $unique_attr ] ) )
		{
			if( !isset( $keys[ $unique_attr ] ) ) return FALSE;
			else $unique_val = $keys[ $unique_attr ];
		}
		else
		{
			$unique_val = $keys[ 'attr:' . $unique_attr ];
		}


		// Look in the EE dummy channel for this unique val
		$entry_id = ee()->shortlist_channel_model->find_create( $unique_val, $keys );

		if( $entry_id === FALSE ) return FALSE;


		return $entry_id;
	}


	// --------------------------------------------------------------

	public function reorder( $items = array() )
	{
		if( empty( $items ) ) return FALSE;

		// Clean the array
		$i = 1;

		foreach( $items as $item )
		{
			ee()->shortlist_core_model->filter_by_current_user();
			self::update( $item, array('item_order' => $i) );

			$i++;
		}

		return TRUE;
	}


	// --------------------------------------------------------------

	public function reassign_to_member( $member_id, $session_id, $merge = FALSE, $default_list_id = '' )
	{
		$data['member_id'] = $member_id;
		$data['session_id'] = '';

		// Get the
		if( $merge == TRUE AND $default_list_id != '' )
		{
			$data['list_id'] = $default_list_id;
		}

		ee()->db->where('session_id', $session_id );
		self::update_group( $data );

		if( $merge == TRUE AND $default_list_id != '' )
		{

			// Check for and remove any duplicates
			$sql = "SELECT a.* FROM exp_shortlist_item a INNER JOIN exp_shortlist_item b ON a.entry_id = b.entry_id WHERE a.member_id = '".$member_id."' AND a.list_id = b.list_id AND a.item_id != b.item_id GROUP BY a.item_id";

			$r = ee()->db->query( $sql )->result_array();

			$arr = array();
			$found = array();

			if( !empty( $r ) )
			{
				// work to do
				foreach( $r as $row )
				{
					if( in_array( $row['entry_id'], $found ) )
					{
						$arr[] = $row['item_id'];
					}
					else
					{
						$found[] = $row['entry_id'];
					}
				}

				if( !empty( $arr ) )
				{
					ee()->shortlist_item_model->delete( $arr, 'item_id');
				}
			}
		}

		return TRUE;
	}

	public function exists_in_list()
	{
		// Get the item id
		$params = ee()->TMPL->tagparams;

		// we need at least
		// 		1. an item id or entry_id
		// 		2. a list id
		$list_id = $this->extract_list_id($params);
		$entry_id = '';

		if($list_id == '') return FALSE;

		if(isset($params['item_id']))
		{
			$entry_id = $this->get_entry_id_from_item_id($params['item_id']);
		}
		elseif( isset($params['entry_id']) )
		{
			$entry_id = $params['entry_id'];
		}

		if($entry_id == '') return FALSE;

		// Get the item from the list
		$item = $this->get_item($entry_id, $list_id);

		if(empty($item) ) return FALSE;

		return TRUE;
	}




	public function extract_list_id($keys = array(), $prefix = '')
	{
		// We're acting on a single list, but passed via a friendly string.
		// We'll need to get the list_id for this
		if(empty($this->lists)) $this->lists = ee()->shortlist_list_model->get_lists();

		if( isset($keys[$prefix.'list_url_title']) )
		{
			foreach($this->lists as $list)
			{
				if($list['list_url_title'] == $keys[$prefix.'list_url_title'] )
				{
					return $list['list_id'];
				}
			}
		}
		elseif( isset($keys[$prefix.'list_name']) )
		{
			foreach($this->lists as $list)
			{
				if($list['list_name'] == $keys[$prefix.'list_name'] )
				{
					return $list['list_id'];
				}
			}
		}
		else
		{
			// we need to handle all three cases,
			// just the _start and _end seperately, but also together
			$start = ( isset($keys[$prefix.'list_url_title_start']) ? $keys[$prefix.'list_url_title_start'] : '' );
			$end = ( isset($keys[$prefix.'list_url_title_end']) ? $keys[$prefix.'list_url_title_end'] : '' );


			foreach($this->lists as $list)
			{
				$start_id = '';
				$end_id = '';
				if( $start != '' AND $list['list_url_title_start'] == $start)
				{
					$start_id = $list['list_id'];

					if( $end == '' ) return $start_id;
				}

				if( $end != '' AND $list['list_url_title_end'] == $end)
				{
					$end_id = $list['list_id'];

					if( $start == '' ) return $end_id;
				}

				if( $start_id == $end_id AND $start_id != '') return $start_id;
			}
		}

		return '';
	}



} // End class

/* End of file Shortlist_project_model.php */