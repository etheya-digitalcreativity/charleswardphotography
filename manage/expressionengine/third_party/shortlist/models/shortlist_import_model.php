<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// include_once config file
include_once PATH_THIRD.'shortlist/config'.EXT;

/**
 * Shortlist Import Model class
 *
 * @package         shortlist_ee_addon
 * @version         2.5.3
 * @author          Joel Bradbury ~ <joel@squarebit.co.uk>
 * @link            http://squarebit.co.uk/shortlist
 * @copyright       Copyright (c) 2013, Joel
 */
class Shortlist_import_model extends Shortlist_model {

	private $members = array();
	private $items = array();

	/**
	 * Constructor
	 *
	 * @access      public
	 * @return      void
	 */
	function __construct()
	{
		parent::__construct();
	}

	// --------------------------------------------------------------------

	/**
	 * Installs given table
	 *
	 * @access      public
	 * @return      void
	 */
	public function install()
	{
		// None
	}


	public function import_from_favorites( $options )
	{
		// Get our bases
		$list_name = $options['list_name'];
		$list_title = $options['list_title'];
		$clear_table = $options['clear_table'];

		// Run this member by member so we don't have to worry about list id issues
		$member_ids = ee()->db->query(" SELECT DISTINCT(member_id) FROM exp_favorites ")->result_array();

		if(empty($member_ids)) return FALSE;

		$keys['list_name'] = $list_name;
		$keys['list_title'] = $list_title;


		foreach($member_ids as $member)
		{
			$sess['key'] = 'member_id';
			$sess['val'] = $member['member_id'];

			// Setup a list for this user
			$list = array();
			$list = ee()->shortlist_list_model->create($keys, TRUE, '', $sess);

			// Get this member's items only, and deal with each
			$member_items = ee()->db->query(" SELECT * FROM exp_favorites WHERE member_id = '".$member['member_id']."' ")->result_array();


			$items = array();
			foreach($member_items as $item)
			{
				// Translate these keys over to our preferred format
				$item['added'] = $item['entry_date'];
				$item['list_id'] = $list['list_id'];

				$items[] = ee()->shortlist_item_model->add( $item, $member['member_id'] );
			}
		}

		// Do we want to clear the table now?
		if($clear_table == 'yes')
		{
			ee()->db->query(" TRUNCATE exp_favorites ");
		}

		return TRUE;
	}





	public function import_from_vc_todo( $options )
	{

		// Get our bases
		$list_name_todo = $options['list_name_todo'];
		$list_title_todo = $options['list_title_todo'];
		$clear_table = $options['clear_table'];
		$list_name_done = $options['list_name_done'];
		$list_title_done = $options['list_title_done'];

		// Run this member by member so we don't have to worry about list id issues
		$member_ids = ee()->db->query(" SELECT DISTINCT(member_id) FROM exp_vc_todo ")->result_array();

		if(empty($member_ids)) return FALSE;

		$keys_todo['list_name'] = $list_name_todo;
		$keys_todo['list_title'] = $list_title_todo;
		$keys_done['list_name'] = $list_name_done;
		$keys_done['list_title'] = $list_title_done;


		foreach($member_ids as $member)
		{
			$sess['key'] = 'member_id';
			$sess['val'] = $member['member_id'];

			// Setup a todo list for this user
			$list = array();
			$list = ee()->shortlist_list_model->create($keys_todo, TRUE, '', $sess);

			// Get this member's items only, and deal with each
			$member_items = ee()->db->query(" SELECT * FROM exp_vc_todo WHERE member_id = '".$member['member_id']."' AND COMPLETE = '0' ")->result_array();

			$items = array();
			foreach($member_items as $item)
			{
				// Translate these keys over to our preferred format
				$item['added'] = $item['entry_date'];
				$item['list_id'] = $list['list_id'];

				$items[] = ee()->shortlist_item_model->add( $item, $member['member_id'] );
			}


			// Same again, but for done
			$list = array();
			$list = ee()->shortlist_list_model->create($keys_done, TRUE, '', $sess);

			// Get this member's items only, and deal with each
			$member_items = ee()->db->query(" SELECT * FROM exp_vc_todo WHERE member_id = '".$member['member_id']."' AND COMPLETE = '1' ")->result_array();

			$items = array();
			foreach($member_items as $item)
			{
				// Translate these keys over to our preferred format
				$item['added'] = $item['entry_date'];
				$item['list_id'] = $list['list_id'];

				$items[] = ee()->shortlist_item_model->add( $item, $member['member_id'] );
			}

		}

		// Do we want to clear the table now?
		if($clear_table == 'yes')
		{
			ee()->db->query(" TRUNCATE exp_vc_todo ");
		}

		return TRUE;
	}




} // End class

/* End of file Shortlist_import_model.php */