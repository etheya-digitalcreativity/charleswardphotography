<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// include_once config file
include_once PATH_THIRD.'shortlist/config'.EXT;

/**
 * Shortlist Core Model class
 *
 * @package         shortlist_ee_addon
 * @version         2.5.3
 * @author          Joel Bradbury ~ <joel@squarebit.co.uk>
 * @link            http://squarebit.co.uk/shortlist
 * @copyright       Copyright (c) 2013, Joel
 */
class Shortlist_core_model extends Shortlist_model {

	// 1 week
	protected $expires = 604800;
	protected $cookie_name = 'shortlist_session_id';

	private $everything = array();
	private $reassign_sessions = TRUE;
	private $reassign_merge_lists = FALSE;
	private $cleanup_data = array();

	/**
	 * Constructor
	 *
	 * @access      public
	 * @return      void
	 */
	function __construct()
	{

		$this->reassign_sessions 		= SHORTLIST_REASSIGN_GUEST_SESSIONS_ON_LOGIN;
		$this->reassign_merge_lists 	= SHORTLIST_REASSIGN_GUEST_SESSIONS_ON_LOGIN_MERGE;

		parent::__construct();
	}

	// --------------------------------------------------------------------

	/**
	 * Installs given table
	 *
	 * @access      public
	 * @return      void
	 */
	public function install()
	{
		// None
	}


	// --------------------------------------------------------------

	public function get_session()
	{
		$key = '';
		$val = '';

		$alt = array();

		// reassign_sessions
		parent::log('Gettting the user\'s session');
		// If the user is logged in, use that
		if( ee()->session->userdata('member_id') != '0' )
		{
			$key = 'member_id';
			$val = ee()->session->userdata('member_id');
			parent::log('Got session via member_id = '.$val );
		}
		elseif( ee()->session->userdata('session_id') != '0' )
		{
			$key = 'session_id';
			$val = ee()->session->userdata('session_id');
			parent::log('Got session via session_id = '.$val );
		}


		if( ($session_val = $this->get_session_ours()) !== FALSE AND $this->reassign_sessions AND $key == 'member_id' )
		{
			$alt['key'] = $this->cookie_name;
			$alt['val'] = $session_val;
			parent::log('Found an older shortlist specific session, against a logged in user, reassigning ownership');
		}


		// None found, create our own
		if( $key == '' )
		{
			// No member_id or session_id. Use our own
			// Create or find
			$key = 'session_id';
			$val = $this->get_create_session();
			parent::log('Got session via shortlist session_id = '.$val );
		}

		if(!empty($alt))
		{

			// Reassign, and clear the lingering session
			$default_list_id = '';
			if( $this->reassign_merge_lists == TRUE )
			{
				$default_list_id = '';
				ee()->db->where('default_list', '1');
				$default_list = ee()->shortlist_list_model->get_one( $val, 'member_id');

				if( !empty($default_list) ) $default_list_id = $default_list['list_id'];
				else $this->reassign_merge_lists = FALSE;
			}

			$cleanup_data = array('member_id' => $val,
									'session_id' => $alt['val'],
									'reassign_merge_lists' => $this->reassign_merge_lists,
									'default_list_id' => $default_list_id );


			ee()->shortlist_list_model->reassign_to_member( $val, $alt['val'], $this->reassign_merge_lists );
			ee()->shortlist_item_model->reassign_to_member( $val, $alt['val'], $this->reassign_merge_lists, $default_list_id );

			// Clear the session
			ee()->functions->set_cookie( $this->cookie_name, FALSE, time() - 36000 );

			ee()->functions->redirect( ee()->functions->fetch_current_uri() );
			exit();

		}

		return array( 'key' => $key, 'val' => $val );
	}


	// --------------------------------------------------------------

	public function get_session_ours()
	{
		// Does the session cookie exist?
		if( ee()->input->cookie( $this->cookie_name ) != FALSE )
		{
			return ee()->input->cookie( $this->cookie_name );
		}

		return FALSE;
	}


	// --------------------------------------------------------------

	public function get_create_session()
	{
		// Does the session cookie exist?
		if( ee()->input->cookie( $this->cookie_name ) != FALSE )
		{
			return ee()->input->cookie( $this->cookie_name );
		}

		// We need to create the cookie
		$session_id = ee()->functions->random();

		ee()->functions->set_cookie( $this->cookie_name, $session_id, $this->expires);

		return $session_id;
	}


	// --------------------------------------------------------------

	public function filter_by_current_user()
	{
		if(isset(ee()->session->userdata)) {

			// If the user is logged in, use that
			if( ee()->session->userdata('member_id') != '0' )
			{
				ee()->db->where('member_id', ee()->session->userdata('member_id'));
			}
			elseif( ee()->session->userdata('session_id') != '0' )
			{
				ee()->db->where('session_id', ee()->session->userdata('session_id'));
			}
			else
			{
				// No member_id or session_id. Use our own
				ee()->db->where('session_id', $this->get_create_session() );
			}

		}

		return;
	}


	// --------------------------------------------------------------------

	/**
	 * Create URL Title
	 *
	 */
	public function validate_url_title($url_title = '', $title = '', $list_id = 0 )
	{
		$update = FALSE;
		if( $list_id != 0 ) $update = TRUE;

		$word_separator = ee()->config->item('word_separator');
		ee()->load->helper('url');

		if ( ! trim($url_title))
		{
			$url_title = url_title($title, $word_separator, TRUE);
		}

		// Remove extraneous characters
		if ($update)
		{
			ee()->db->select('list_url_title');
			$url_query = ee()->db->get_where('shortlist_list', array('list_id' => $list_id));

			if ($url_query->row('list_url_title') != $url_title)
			{
				$url_title = url_title($url_title, $word_separator);
			}
		}
		else
		{
			$url_title = url_title($url_title, $word_separator);
		}


		if ($update)
		{
			$url_title = $this->_unique_url_title($url_title, $list_id);
		}
		else
		{
			$url_title = $this->_unique_url_title($url_title, '');
		}


		return $url_title;
	}


	// --------------------------------------------------------------------

	/**
	 * Create URL Title
	 *
	 */
	public function validate_url_title_end($url_title = '', $start = '', $list_id = FALSE )
	{
		$update = TRUE;
		if( $list_id === FALSE ) $update = FALSE;

		$word_separator = ee()->config->item('word_separator');
		ee()->load->helper('url');

		// Remove extraneous characters
		if ($update)
		{

			ee()->db->select( array('list_url_title_start', 'list_url_title_end'));
			$url_query = ee()->db->get_where('shortlist_list', array('list_id' => $list_id));

			if ($url_query->row('list_url_title_end') != $url_title)
			{
				$url_title = url_title($url_title, $word_separator);
			}
		}
		else
		{
			$url_title = url_title($url_title, $word_separator);
		}



		if ($update)
		{
			$url_title = $this->_unique_url_title($url_title, $list_id, $start);
		}
		else
		{
			$url_title = $this->_unique_url_title($url_title, '', $start);
		}

		return $url_title;
	}


	// --------------------------------------------------------------------

	/**
	 * Unique URL Title
	 *
	 */
	protected function _unique_url_title($url_title, $self_id, $start = '')
	{

		$table = 'shortlist_list';
		$url_title_field = 'list_url_title';
		if( $start != '') $url_title_field = 'list_url_title_end';
		$self_field = 'list_id';

		// Field is limited to 100 characters, so trim url_title before querying
		$url_title = substr($url_title, 0, 100);

		if ($self_id != '')
		{
			ee()->db->where(array($self_field.' !=' => $self_id));
		}

		if( $start != '' )
		{
			ee()->db->where(array('list_url_title_start' => $start));
		}

		ee()->db->where(array($url_title_field => $url_title) );

		$count = ee()->db->count_all_results($table);

		if ($count > 0)
		{
			// We may need some room to add our numbers- trim url_title to 70 characters
			$url_title = substr($url_title, 0, 70);

			// Check again
			if ($self_id != '')
			{
				ee()->db->where(array($self_field.' !=' => $self_id));
			}

			if( $start != '' )
			{
				ee()->db->where(array('list_url_title_start' => $start));
			}

			ee()->db->where(array($url_title_field => $url_title));
			$count = ee()->db->count_all_results($table);

			if ($count > 0)
			{
				if ($self_id != '')
				{
					ee()->db->where(array($self_field.' !=' => $self_id));
				}

				if( $start != '' )
				{
					ee()->db->where(array('list_url_title_start' => $start));
				}

				ee()->db->select("{$url_title_field}, MID({$url_title_field}, ".(strlen($url_title) + 1).") + 1 AS next_suffix", FALSE);
				ee()->db->where("{$url_title_field} REGEXP('".preg_quote(ee()->db->escape_str($url_title))."[0-9]*$')");
				ee()->db->order_by('next_suffix', 'DESC');
				ee()->db->limit(1);
				$query = ee()->db->get($table);

				// Did something go tragically wrong?  Is the appended number going to kick us over the 75 character limit?
				if ($query->num_rows() == 0 OR ($query->row('next_suffix') > 99999))
				{
					return FALSE;
				}

				$url_title = $url_title.$query->row('next_suffix');

				// little double check for safety

				if ($self_id != '')
				{
					ee()->db->where(array($self_field.' !=' => $self_id));
				}

				if( $start != '' )
				{
					ee()->db->where(array('list_url_title_start' => $start));
				}

				ee()->db->where(array($url_title_field => $url_title));
				$count = ee()->db->count_all_results($table);

				if ($count > 0)
				{
					return FALSE;
				}
			}
		}

		return $url_title;
	}

	public function everything()
	{
		if( !empty( $this->everything ) ) return $this->everything;

		$items = ee()->shortlist_item_model->get_all();
		$lists = ee()->shortlist_list_model->get_lists();

		if( empty( $lists ) ) return array();

		$data['lists'] = array();
		$l = array();
		foreach( $lists as $list )
		{
			$data['lists'][ $list['list_id'] ] = $list;
			$l[ $list['list_id'] ] = $list;
		}


		$tmp = array();
		foreach( $items as $item )
		{
			if( !isset( $tmp[$item['entry_id']] ) ) $tmp[$item['entry_id']] = array();
			if( !isset( $l[ $item['list_id'] ] ) ) continue;

			$data['lists'][$item['list_id']]['items'][] = $item;
			$item['list'] = $l[ $item['list_id'] ];

			$tmp[$item['entry_id']][] = $item;

		}

		$data['items'] = $tmp;

		$this->everything = $data;

		return $this->everything;
	}

	public function check_yes_no( $type = 'yes', $value = '')
	{
		if($value == '') return FALSE;

		$yes_arr = array('yes','y','on','true','t');
		$no_arr = array('no','n','off','false','f');

		if( $type == 'yes' )
		{
			if( in_array($value, $yes_arr) ) return TRUE;
			return FALSE;
		}
		else
		{
			if( in_array($value, $no_arr) ) return TRUE;
			return FALSE;
		}
	}


	/*
	* Flush DB
	*
	* The native flush db functions miss out the order_by details
	* This fixes that and replicates the more complete private _reset_write()
	* call in db_active_record.php
	*/
	public function flush_db()
	{
		ee()->db->flush_cache();

		$ar_reset_items = array(
								'ar_set'		=> array(),
								'ar_from'		=> array(),
								'ar_where'		=> array(),
								'ar_like'		=> array(),
								'ar_orderby'	=> array(),
								'ar_keys'		=> array(),
								'ar_limit'		=> FALSE,
								'ar_order'		=> FALSE
								);

		foreach ($ar_reset_items as $item => $default_value)
		{
			ee()->db->$item = $default_value;
		}
	}



} // End class

/* End of file Shortlist_core_model.php */