<?php

/**
 * Shortlist Config File *
 * @package         shortlist_ee_addon
 * @version         2.5.3
 * @author          Joel Bradbury ~ <joel@squarebit.co.uk>
 * @link            http://squarebit.co.uk/shortlist
 * @copyright       Copyright (c) 2013, Joel
 */

if ( ! defined('SHORTLIST_NAME'))
{
	define('SHORTLIST_NAME',         'Shortlist');
	define('SHORTLIST_CLASS_NAME',   'Shortlist');
	define('SHORTLIST_VERSION',      '2.5.3');
	define('SHORTLIST_DOCS',         'http://squarebit.co.uk/shortlist');
	define('SHORTLIST_DEBUG',        TRUE);


	define('SHORTLIST_CHANNEL_NAME',  		'shortlist_external_entries');
	define('SHORTLIST_CHANNEL_LABEL', 		'Shortlist External Entries');
	define('SHORTLIST_GROUP_NAME', 			'Shortlist External Entries Fields');
	define('SHORTLIST_DATABLOCK_FIELD_NAME','shortlist_datablock');
	define('SHORTLIST_UNIQUE_FIELD_NAME', 	'shortlist_unique_val');

	// defines the default merge behaviour. Unless you don't want this merge option,
	// just leave this as is
	define('SHORTLIST_REASSIGN_GUEST_SESSIONS_ON_LOGIN', 		FALSE);
	// Change this to false if you're using only single lists on your site, but still
	// want the merge option enabled
	define('SHORTLIST_REASSIGN_GUEST_SESSIONS_ON_LOGIN_MERGE', 	FALSE);
}

$config['name']    = SHORTLIST_NAME;
$config['version'] = SHORTLIST_VERSION;
