<?php defined('BASEPATH') or exit('No direct script access allowed');


class Shortlist_mcp
{
	public $module_name;
	private $nocache;
	private $cache;

	function __construct()
	{
		//--------------------------------------------
		//	Alias to get_instance()
		//--------------------------------------------
		if ( ! function_exists('ee') )
		{
			function ee()
			{
				return get_instance();
			}
		}

		$this->module_name = strtolower(str_replace('_mcp', '', get_class($this)));
		$this->base = str_replace( '&amp;D=', '&D=', BASE.'&C=addons_modules&M=show_module_cp&module=' . $this->module_name );


		$this->contols[]  = $this->base.AMP.'method=settings';


		$controls = array(  lang('shortlist')		=> $this->base . '&method=index',
							lang('import')			=> $this->base . '&method=extras');

		ee()->cp->set_right_nav( $controls );


		// Load helper
		ee()->load->helper('Shortlist');

		// Load Shortlist base model
		if( !isset( ee()->shortlist_model ) ) ee()->load->library('Shortlist_model');

		// Load other models
		Shortlist_model::load_models();
	}


	function index()
	{
		ee()->cp->set_variable('cp_page_title', lang('shortlist_module_name'));

		$this->_add_morphine();

		$this->cached = array();
		// Get all the items

		$this->_get_lists();
		$this->_get_items();
		$this->_get_members();

		return ee()->load->view('mcp_index', $this->cache, TRUE);

	}

	// --------------------------------------------------------------------

	function extras()
	{
		ee()->cp->set_variable('cp_page_title', lang('shortlist_import_title'));

		$this->_add_morphine();

		$this->cached = array();

		$this->cache['show_favorites_option'] = FALSE;
		$this->cache['show_vc_todo_option'] = FALSE;

		$this->cache['import_favs_uri'] = $this->base . '&method=import_favourites';
		$this->cache['import_custom_uri'] = $this->base . '&method=import_vc_todo';

		// Do we have favorites data?
		$f = ee()->db->query(" SHOW TABLES LIKE 'exp_favorites' ")->result_array();
		if( count($f) > 0)
		{
			$f_r = ee()->db->query(" SELECT COUNT(*) c FROM exp_favorites ")->row_array();
			if( $f_r['c'] > 0 )
			{
				$this->cache['show_favorites_option'] = TRUE;
			}
		}

		// Do we have vc_todo data?

		$f = ee()->db->query(" SHOW TABLES LIKE 'exp_vc_todo' ")->result_array();
		if( count($f) > 0)
		{
			$f_r = ee()->db->query(" SELECT COUNT(*) c FROM exp_vc_todo ")->row_array();
			if( $f_r['c'] > 0 )
			{
				$this->cache['show_vc_todo_option'] = TRUE;
			}
		}



		$this->cache['show_favorites_option'] = TRUE;
		return ee()->load->view('mcp_extras', $this->cache, TRUE);

	}

	// --------------------------------------------------------------------




	function import_complete()
	{
		ee()->view->cp_page_title = lang('shortlist_import_complete_title');

		$this->_add_morphine();

		$this->cached = array();

		return ee()->load->view('mcp_import_complete', $this->cache, TRUE);

	}

	// --------------------------------------------------------------------


	function import_favourites()
	{
		ee()->view->cp_page_title = lang('shortlist_import_favourites');
		$this->_add_morphine();
		$this->cache = array();
		$this->cache['show_confirm'] = TRUE;
		$this->cache['list_name'] = '';
		$this->cache['list_title'] = '';
		$this->cache['clear_table'] = 'yes';


		if(!empty($_POST))
		{
			// Posted, handle;
			$this->cache['show_confirm'] = FALSE;

			// Validate the passed data
			$required = array('list_title', 'list_name', 'clear_table');
			$data = array();
			$errors = array();
			foreach($required as $req)
			{
				if(!isset($_POST[$req]) OR $_POST[$req] == '') $errors[$req] = 'missing';
				else
				{
					$data[$req] = $_POST[$req];
					$this->cache[$req] = $_POST[$req];
				}
			}

			if(empty($errors))
			{
				// Good to go.
				$this->_import_from_favorites($data);
				return;
			}

			$this->cache['errors'] = $errors;
		}

		// Get some stats on the favourites data
		$this->cache['stats'] = $this->_get_stats_favourites();



		$this->cache['form_post_url'] = 'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->module_name.'&method=import_favourites&act=run';

		return ee()->load->view('mcp_import_favourites', $this->cache, TRUE);

	}
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------


	function import_vc_todo()
	{
		ee()->view->cp_page_title = lang('shortlist_import_vc_todo');
		$this->_add_morphine();
		$this->cache = array();
		$this->cache['show_confirm'] = TRUE;
		$this->cache['list_name_todo'] = '';
		$this->cache['list_title_todo'] = '';

		$this->cache['list_title_done'] = '';
		$this->cache['list_name_done'] = '';

		$this->cache['clear_table'] = 'yes';


		if(!empty($_POST))
		{
			// Posted, handle;
			$this->cache['show_confirm'] = FALSE;

			// Validate the passed data
			$required = array('list_title_todo', 'list_name_todo', 'list_title_done', 'list_name_done', 'clear_table');
			$data = array();
			$errors = array();
			foreach($required as $req)
			{
				if(!isset($_POST[$req]) OR $_POST[$req] == '') $errors[$req] = 'missing';
				else
				{
					$data[$req] = $_POST[$req];
					$this->cache[$req] = $_POST[$req];
				}
			}

			if(empty($errors))
			{
				// Good to go.
				$this->_import_from_vc_todo($data);
				return;
			}

			$this->cache['errors'] = $errors;
		}

		// Get some stats on the favourites data
		$this->cache['stats'] = $this->_get_stats_vc_todo();


		$this->cache['form_post_url'] = 'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->module_name.'&method=import_vc_todo&act=run';

		return ee()->load->view('mcp_import_vc_todo', $this->cache, TRUE);

	}
	// --------------------------------------------------------------------


	function view_list()
	{
		ee()->view->cp_page_title = lang('shortlist_list_page');

		ee()->cp->set_breadcrumb($this->base, lang('shortlist_module_name'));
		$this->_add_morphine();

		$this->cached = array();
		// Get just this list

		$list_id = ee()->input->get('list_id');

		$this->_get_lists( $list_id );
		$this->_get_items_for_list( $list_id );
		$this->_get_clones( $list_id );
		$this->_get_members();


		return ee()->load->view('mcp_list', $this->cache, TRUE);
	}

	// --------------------------------------------------------------------

	function view_item()
	{
		ee()->view->cp_page_title = lang('shortlist_item_page');

		ee()->cp->set_breadcrumb($this->base, lang('shortlist_module_name'));
		$this->_add_morphine();

		$this->cached = array();
		// Get just this list

		$entry_id = ee()->input->get('entry_id');
		$this->_get_items( $entry_id );
		$this->_get_lists_with_entry( $entry_id );
		$this->_get_members();


		return ee()->load->view('mcp_item', $this->cache, TRUE);
	}
	// --------------------------------------------------------------------

	function settings()
	{
		// --------------------------------------
		// Load some libraries
		// --------------------------------------

		ee()->load->library('javascript');

		ee()->view->cp_page_title = lang('settings');
		ee()->cp->set_breadcrumb($this->base, lang('shortlist_module_name'));

		$this->cached_vars['form_post_url'] = $this->base . '&method=save_settings';

		return ee()->load->view('settings', $this->cached_vars, TRUE);
	}


	private function _get_clones( )
	{
		$clones = array();
		if( isset( $this->cache['items'] ) AND !empty( $this->cache['items'] ) )
		{
			// Get the list id from the items
			$row = current( $this->cache['items'] );
			$list_id = $row['list_id'];

			$clones = ee()->shortlist_item_model->get_clones( $list_id );

			// Cleanup
			foreach( $clones as $key => $clone )
			{
				if( $clone['member_id'] != '0' )
				{
					$clones[ $key ]['user_type'] = 'member';

					// Add this member_id to a temp arrary so we can pull the required member details
					// in a single query
					$this->cache['members'][ $clone['member_id'] ] = array();
				}
				else
				{
					$clones[ $key ]['user_type'] = 'guest';
				}

				$clones[$key]['list_detail_url'] = $this->base . '&method=view_list&list_id=' . $clone['list_id'];

				$clones[$key]['item_detail_url'] = $this->base . '&method=view_item&entry_id=' . $clone['entry_id'];

				$clones[$key]['added_formatted'] = ee()->localize->set_human_time($clone['added']);
				$clones[$key]['added_since'] = $this->_time_elapsed_string( ee()->localize->now - $clone['added']);
			}

		}

		$this->cache['clones'] = $clones;
		$this->cache['clones_count'] = count( $clones );
	}

	private function _get_lists_with_entry( $entry_id )
	{
		$lists = ee()->shortlist_item_model->get_lists_with_entry( $entry_id );

		// Cleanup the lists
		foreach( $lists as $key => $list )
		{
			if( $list['member_id'] != '0' )
			{
				$lists[ $key ]['user_type'] = 'member';

				// Add this member_id to a temp arrary so we can pull the required member details
				// in a single query
				$this->cache['members'][ $list['member_id'] ] = array();
			}
			else
			{
				$lists[ $key ]['user_type'] = 'guest';
			}

			$lists[$key]['total_item_count'] = $list['c'];
			$lists[$key]['last_activity_formatted'] = ee()->localize->set_human_time($list['added']);
			$lists[$key]['last_activity_since'] = $this->_time_elapsed_string( ee()->localize->now - $list['added']);
			$lists[$key]['list_detail_url'] = $this->base . '&method=view_list&list_id=' . $list['list_id'];
		}

		$this->cache['list_count'] = count( $lists );
		$this->cache['lists'] = $lists;
	}

	private function _get_members()
	{
		if( ! isset( $this->cache['members'] ) OR empty( $this->cache['members']  ) ) return;

		$member_ids = array_keys( $this->cache['members'] );

		// Now pull the member details from the db
		$members = ee()->db->select('member_id, group_id, username, screen_name, email')
						->where_in('member_id', $member_ids )
						->get('members')
						->result_array();

		foreach( $members as $member )
		{
			if( isset( $this->cache['members'][ $member['member_id'] ] ) )
			{
				$this->cache['members'][ $member['member_id'] ] = $member;
			}
		}

	}

	private function _get_items( $entry_id = '' )
	{

		if( $entry_id == '' ) $items = ee()->shortlist_item_model->get_all_items();
		else $items = ee()->shortlist_item_model->get_one_by_entry_id( $entry_id );

		$internal_count = 0;
		$external_count = 0;

		foreach( $items as $key => $item )
		{
			$type = 'Internal';
			if( isset( $item['unique_val'] ) ) $type = 'External';
			$items[ $key ]['type'] = $type;

			if( $type == 'Internal' )
			{
				$internal_count++;
				$items[ $key ]['entry_edit_url'] = BASE. '&C=content_publish&M=entry_form&entry_id='.$item['entry_id'];

			}
			if( $type == 'External' ) $external_count++;


			$items[$key]['item_detail_url'] = $this->base . '&method=view_item&entry_id=' . $item['entry_id'];

			$items[$key]['added_formatted'] = ee()->localize->set_human_time($item['added']);
			$items[$key]['added_since'] = $this->_time_elapsed_string( ee()->localize->now - $item['added']);
		}

		$this->cache['item_count'] = count( $items );
		$this->cache['internal_count'] = $internal_count;
		$this->cache['external_count'] = $external_count;
		$this->cache['items'] = $items;

		if( $entry_id != '' )
		{
			// This is a single item, we also want to get all the item
			// attributes as passed when added if this is an external item
			$item = current( $items );
			if( $item['type'] == 'External' )
			{
				$item_details = ee()->shortlist_channel_model->get_data( $item['entry_id'] );

				$item['meta'] = $item_details;
			}


			$this->cache['item'] = $item;
		}

	}


	private function _get_items_for_list( $list_id = '' )
	{

		if( $list_id == '' ) $items = ee()->shortlist_item_model->get_all_items();
		else $items = ee()->shortlist_item_model->get_list( $list_id, TRUE );

		$internal_count = 0;
		$external_count = 0;

		foreach( $items as $key => $item )
		{
			$type = 'Internal';
			if( isset( $item['unique_val'] ) ) $type = 'External';
			$items[ $key ]['type'] = $type;

			if( $type == 'Internal' ) $internal_count++;
			if( $type == 'External' ) $external_count++;

			$items[$key]['item_detail_url'] = $this->base . '&method=view_item&entry_id=' . $item['entry_id'];

			$items[$key]['added_formatted'] = ee()->localize->set_human_time($item['added']);
			$items[$key]['added_since'] = $this->_time_elapsed_string( ee()->localize->now - $item['added']);

		}

		$this->cache['item_count'] = count( $items );
		$this->cache['internal_count'] = $internal_count;
		$this->cache['external_count'] = $external_count;
		$this->cache['items'] = $items;

	}



	private function _get_lists( $list_id = '' )
	{
		if( $list_id == '' ) $lists = ee()->shortlist_item_model->get_all_lists();
		else $lists = ee()->shortlist_item_model->get_list_single( $list_id );

		// Cleanup the lists
		foreach( $lists as $key => $list )
		{
			if( $list['member_id'] != '0' )
			{
				$lists[ $key ]['user_type'] = 'member';

				// Add this member_id to a temp arrary so we can pull the required member details
				// in a single query
				$this->cache['members'][ $list['member_id'] ] = array();
			}
			else
			{
				$lists[ $key ]['user_type'] = 'guest';
			}


			$lists[$key]['last_activity_formatted'] = ee()->localize->set_human_time($list['added']);
			$lists[$key]['last_activity_since'] = $this->_time_elapsed_string( ee()->localize->now - $list['added']);
			$lists[$key]['list_detail_url'] = $this->base . '&method=view_list&list_id=' . $list['list_id'];
		}

		$this->cache['list_count'] = count( $lists );
		$this->cache['lists'] = $lists;

		if( $list_id != '' ) $this->cache['list'] = current( $lists );

	}

	public function save_settings()
	{
		$data = array();

		foreach( ee()->shortlist_example_model->attributes() as $attribute )
		{
			if( ee()->input->get_post( $attribute ) != '' )
			{
				$data[ $attribute ] = ee()->input->get_post( $attribute );
			}
		}

		ee()->shortlist_example_model->insert( $data );

        // ----------------------------------
        //  Redirect to Settings page with Message
        // ----------------------------------

        ee()->functions->redirect($this->base . '&method=settings&msg=preferences_updated');
        exit;

	}



	private function _add_morphine()
	{
		$theme_folder_url = ee()->config->item('theme_folder_url');

		if (substr($theme_folder_url, -1) != '/') {
			$theme_folder_url .= '/';
		}

		$theme_folder_url .= "third_party/shortlist/";

		ee()->cp->add_to_head('<link rel="stylesheet" type="text/css" href="'.$theme_folder_url.'styles/screen.css" />');

		ee()->cp->add_to_head('<script type="text/javascript" charset="utf-8" src="'.$theme_folder_url.'scripts/compressed.js"></script>');
		//ee()->cp->add_to_head('<script type="text/javascript" charset="utf-8" src="'.$theme_folder_url.'shortlist.js"></script>');


	}




	/**
	 * Time String
	 *
	 * Returns the relative time in a nicer wordy fashion
	 *
	 * @access	private
	 * @return	string
	 */

	private function _time_elapsed_string($ptime, $future = FALSE )
	{

		$ago 		= ee()->lang->line('period_ago');

		if( $ptime < 0 )
		{
			$etime = $ptime * -1;
			$ago = 'left';
		}
		else
		{
			$etime = $ptime;
		}

	    if ($etime < 1 AND $future == FALSE )  return ee()->lang->line('period_now');

	    $a = array( 12 * 30 * 24 * 60 * 60  	=>  'period_year',
	                   30 * 24 * 60 * 60        =>  'period_month',
	                   24 * 60 * 60             =>  'period_day',
	                   60 * 60                  =>  'period_hour',
	                   60                       =>  'period_min',
	                   1                        =>  'period_sec');

	     foreach ($a as $secs => $str)
	     {
	         $d = $etime / $secs;

			 if ($d >= 1)
			 {
				if($secs == 60)
				{
					$str = 'period_min';
				}

		        $r = round($d);

		        $str = $str . ($r > 1 ? 's' : '');
		        return $r . ' ' . ee()->lang->line( $str ) . ' ' . $ago;
	       	}
	     }


    }



    private function _get_stats_vc_todo()
    {
    	$stats = array();

    	$r = ee()->db->query("SELECT COUNT(*) c FROM exp_vc_todo")->row_array();
    	$stats['count'] = $r['c'];


    	$r = ee()->db->query("SELECT COUNT(DISTINCT member_id) m FROM exp_vc_todo")->row_array();
    	$stats['members'] = $r['m'];

    	return $stats;
    }


    private function _get_stats_favourites()
    {
    	$stats = array();

    	$r = ee()->db->query("SELECT COUNT(*) c FROM exp_favorites")->row_array();
    	$stats['count'] = $r['c'];


    	$r = ee()->db->query("SELECT COUNT(DISTINCT member_id) m FROM exp_favorites")->row_array();
    	$stats['members'] = $r['m'];


    	return $stats;
    }


    private function _import_from_favorites($data)
    {
    	// We're importing.
    	// We'll pass over to the core model to the actual work, then redirect as appropriate afterwards here

    	// We only really want to load this model when we need to use it. Do it now
		ee()->load->model("shortlist_import_model");

    	$state = ee()->shortlist_import_model->import_from_favorites($data);

    	if($state !== FALSE)
    	{
    		// It worked.
       		ee()->functions->redirect($this->base . '&method=import_complete&msg=import_complete');
       		exit();
    	}

		// Something went wrong
   		ee()->functions->redirect($this->base . '&method=import_complete&msg=error');
   		exit;
    }



    private function _import_from_vc_todo($data)
    {
    	// We're importing.
    	// We'll pass over to the core model to the actual work, then redirect as appropriate afterwards here

    	// We only really want to load this model when we need to use it. Do it now
		ee()->load->model("shortlist_import_model");

    	$state = ee()->shortlist_import_model->import_from_vc_todo($data);

    	if($state !== FALSE)
    	{
    		// It worked.
       		ee()->functions->redirect($this->base . '&method=import_complete&msg=import_complete');
       		exit();
    	}

		// Something went wrong
   		ee()->functions->redirect($this->base . '&method=import_complete&msg=error');
   		exit;
    }






}
