<div id="shortlist_container" class="mor">


	<div class="tg" style="">
		<h2>Import Existing Data from <em>Solspace's Favorites</em></h2>


		<?php if($show_confirm) : ?>

		<div class="alert error import_backup_first">
			<strong>!!! Safety first</strong> - Before you start, have you made a database backup?
			<a href="#" id="import_backup" class="btn add">I Have a Backup</a>
		</div>

		<?php endif; ?>


		<div class="import_main">

			<div class="alert">
				There are <strong><?=$stats['count']?></strong> items to import, from <strong><?=$stats['members']?></strong> members.<br/>
				We need to set a few options for the import 
			</div>


			<?=form_open($form_post_url);?>
			<table class="data">
				
				<tbody>
					<tr>
						<td style="width:50%">
							<label for="list_title">List Title<br/>
							<strong>{list_title}</strong></label>
						</td>
						<td><input id="list_title" name="list_title" type="text" value="<?=$list_title?>"  <?php if(isset($errors['list_title'])) : ?>class="error"<?php endif; ?>/>
							<?php if(isset($errors['list_title'])) : ?>
								<strong>* Required</strong>
							<?php endif; ?></td>
					</tr>
					<tr>
						<td>
							<label for="list_name">List Name<br/>
							<strong>{list_name}</strong></label>
						</td>
						<td><input type="text" id="list_name" name="list_name" value="<?=$list_name?>" <?php if(isset($errors['list_name'])) : ?>class="error"<?php endif; ?>/>
							<?php if(isset($errors['list_name'])) : ?>
								<strong>* Required</strong>
							<?php endif; ?>
						</td>
					</tr>
					<tr>
						<td>
							<label for="clear_table">Clear Favorites table when done?<br/>
							<em>This will empty the converted data from the <strong>exp_favorites</strong> table, but leave the table itself in place.</em></label></td>
						<td>
							<select name="clear_table" id="clear_table">
								<option value="yes" <?php if($clear_table == 'yes') : ?>
								selected="selected"
							<?php endif; ?> >Yes, Empty the table</option>
								<option value="no" <?php if($clear_table == 'no') : ?>
								selected="selected"
							<?php endif; ?> >No, Leave the data as is</option>
							</select>
							<?php if(isset($errors['clear_table'])) : ?>
								<strong>* Required</strong>
							<?php endif; ?>


						</td>
					</tr>
					<tr>
						<td></td>
						<td>
							<input type="submit" class="btn add" value="Import"/>
							This may take a minute
						</td>
					</tr>
				</tbody>

			</table>
			<?=form_close()?>
		</div>


	</div>
	

</div>

	
	<?php if($show_confirm) : ?>

    <script type="text/javascript">
        $(document).ready(function(){

        	$('.import_main').hide();

        	$('#import_backup').on('click', function(){ 
        		$('.import_main').fadeIn();
        		$('.import_backup_first').hide();

        		return false;
        	});


        });
    </script>

    <?php endif; ?>