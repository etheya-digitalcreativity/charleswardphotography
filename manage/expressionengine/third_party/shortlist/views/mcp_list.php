<div id="shortlist_container" class="mor">

	

	<div class="tg" style="width:49%; float:left; margin-right: 1%;">
		<h2><?=lang('shortlist_details_about_this_list')?></h2>

		<table class="data">
			<tbody>
				<tr>
					<td style="text-align: right"><?=lang('shortlist_owned_by')?></td>
					<td><strong><?php if( $list['user_type'] == 'guest' ) : ?>[ <?=lang('shortlist_guest')?> ]
						<?php else : ?><?=$members[ $list['member_id'] ]['screen_name']?><?php endif;?></strong></td>
				</tr>
				<tr>
					<td style="text-align: right"><?=lang('shortlist_share_id')?></td>
					<td><strong><?=$list['list_id']?></strong></td>
				</tr>
				<tr>
					<td style="text-align: right"><?=lang('item_count')?></td>
					<td><strong><?=$item_count?></strong></td>
				</tr>
				<tr>
					<td style="text-align: right"><?=lang('shortlist_last_active')?></td>
					<td><strong><?=$list['last_activity_since']?></strong></td>
				</tr>
			</tbody>
		</table>
	</div>


	<div style="width:49%; float:left;">
		<div class="tg">
			<h2><?=lang('shortlist_list_has')?> <?=$item_count?> <?=lang_switch('shortlist_item', $item_count )?></h2>

			<table class="data">
				<thead>
					<tr style="background-color :transparent">
						<th><?=lang('shortlist_title')?></th>
						<th><?=lang('type')?></th>
						<th><?=lang('shortlist_added')?></th>
					</tr>
				</head>
				<tbody>

					<?php foreach( $items as $item ) : ?>
					<tr>
						<td><a href="<?=$item['item_detail_url']?>"><?=$item['title']?></a></td>
						<td><?=lang('shortlist_'.$item['type'])?></td>
						<td><?=$item['added_since']?></td>
					</tr>
					<?php endforeach; ?>

				</tbody>

			</table>
		</div>

		<?php if( $clones_count > 0 ) : ?>
		<div class="tg">
			<h2><?=lang('shortlist_items_have_been_cloned')?> <?=$clones_count?> <?=lang_switch('shortlist_times', $clones_count )?></h2>

			<table class="data">
				<thead>
					<tr style="background-color :transparent">
						<th><?=lang('title')?></th>
						<th><?=lang('shortlist_cloned_by')?></th>
						<th><?=lang('shortlist_when')?></th>
					</tr>
				</head>
				<tbody>
					<?php foreach( $clones as $clone ) : ?>
					<tr>
						<td><a href="<?=$clone['item_detail_url']?>"><?=$clone['title']?></a></td>
						<td> &#8594; &nbsp; <a href="<?=$clone['list_detail_url']?>"><?php if( $clone['user_type'] == 'guest' ) : ?>[ <?=lang('shortlist_guest')?> ]
						<?php else : ?><?=$members[ $clone['member_id'] ]['screen_name']?><?php endif;?></a></td>
						<td><?=$clone['added_since']?></td>
					</tr>
					<?php endforeach; ?>
				</tbody>

			</table>
		</div>

		<?php endif; ?>
	</div>
	

</div>