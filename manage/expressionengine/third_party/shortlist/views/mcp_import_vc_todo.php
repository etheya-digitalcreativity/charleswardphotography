<div id="shortlist_container" class="mor">


	<div class="tg" style="">
		<h2>Import Existing Data from <em>VC Todo</em></h2>


		<?php if($show_confirm) : ?>

		<div class="alert error import_backup_first">
			<strong>!!! Safety first</strong> - Before you start, have you made a database backup?
			<a href="#" id="import_backup" class="btn add">I Have a Backup</a>
		</div>

		<?php endif; ?>


		<div class="import_main">

			<div class="alert">
				There are <strong><?=$stats['count']?></strong> items to import, from <strong><?=$stats['members']?></strong> members.<br/>
				We need to set a few options for the import 
			</div>


			<?=form_open($form_post_url);?>
			<table class="data">
				
				<tbody>
					<tr>
						<td colspan="2" style="width:50%"><strong>Todo List</strong></td>
						<td colspan="2"><strong>Done List</strong></td>
					</tr>
					<tr>
						<td>
							<label for="list_title_todo">List Title<br/>
							<strong>{list_title}</strong></label>
						</td>
						<td><input id="list_title_todo" name="list_title_todo" type="text" value="<?=$list_title_todo?>"  <?php if(isset($errors['list_title_todo'])) : ?>class="error"<?php endif; ?>/>
							<?php if(isset($errors['list_title_todo'])) : ?>
								<strong>* Required</strong>
							<?php endif; ?></td>

						<td>
							<label for="list_title_done">List Title<br/>
							<strong>{list_title}</strong></label>
						</td>
						<td><input id="list_title_done" name="list_title_done" type="text" value="<?=$list_title_done?>"  <?php if(isset($errors['list_title_done'])) : ?>class="error"<?php endif; ?>/>
							<?php if(isset($errors['list_title_done'])) : ?>
								<strong>* Required</strong>
							<?php endif; ?></td>
					</tr>
					<tr>
						<td>
							<label for="list_name_todo">List Name<br/>
							<strong>{list_name}</strong></label>
						</td>
						<td><input type="text" id="list_name_todo" name="list_name_todo" value="<?=$list_name_todo?>" <?php if(isset($errors['list_name_todo'])) : ?>class="error"<?php endif; ?>/>
							<?php if(isset($errors['list_name_todo'])) : ?>
								<strong>* Required</strong>
							<?php endif; ?>
						</td>

						<td>
							<label for="list_name_done">List Name<br/>
							<strong>{list_name}</strong></label>
						</td>
						<td><input type="text" id="list_name_done" name="list_name_done" value="<?=$list_name_done?>" <?php if(isset($errors['list_name_done'])) : ?>class="error"<?php endif; ?>/>
							<?php if(isset($errors['list_name_done'])) : ?>
								<strong>* Required</strong>
							<?php endif; ?>
						</td>
					</tr>

					<tr>
						<td colspan="2">
							<label for="clear_table">Clear VC Todo table when done?<br/>
							<em>This will empty the converted data from the <strong>exp_vc_todo</strong> table, but leave the table itself in place.</em></label></td>
						<td colspan="2">
							<select name="clear_table" id="clear_table">
								<option value="yes" <?php if($clear_table == 'yes') : ?>
								selected="selected"
							<?php endif; ?> >Yes, Empty the table</option>
								<option value="no" <?php if($clear_table == 'no') : ?>
								selected="selected"
							<?php endif; ?> >No, Leave the data as is</option>
							</select>
							<?php if(isset($errors['clear_table'])) : ?>
								<strong>* Required</strong>
							<?php endif; ?>


						</td>
					</tr>
					<tr>
						<td colspan="2"></td>
						<td colspan="2">
							<input type="submit" class="btn add" value="Import"/>
							This may take a minute
						</td>
					</tr>
				</tbody>

			</table>
			<?=form_close()?>
		</div>


	</div>
	

</div>

	
	<?php if($show_confirm) : ?>

    <script type="text/javascript">
        $(document).ready(function(){

        	$('.import_main').hide();

        	$('#import_backup').on('click', function(){ 
        		$('.import_main').fadeIn();
        		$('.import_backup_first').hide();

        		return false;
        	});


        });
    </script>

    <?php endif; ?>