<?php defined('BASEPATH') or exit('No direct script access allowed');

$lang = array(

	'shortlist_module_name'			=> 'Shortlist',
	'shortlist_module_description'	=> 'Create lists painlessly',

	'unique_val_field_label' 	=> 'Unique Value',
	'unique_val_field_desc'	=> 'A unqiue value for this entry as defined when the item was added to a shortlist',

	'datablock_field_label'	=> 'Datablock',
	'datablock_field_desc'	=> 'Complete data for this item in array form',


	// CP Titles
	'shortlist_item_page' => 'Item',
	'shortlist_list_page' => 'List',
	'shortlist' => 'Shortlist',
	'settings' => 'Settings',
	'import' => 'Import',
	'extras' => 'Extras',
	'shortlist_import_title' => 'Shortlist - Import',
	'shortlist_import_complete_title'	 => 'Shortlist - Import Complete',
	'shortlist_import_favourites'	=> 'Shortlist - Import from Favorites',
	'shortlist_import_vc_todo'	 => 'Shortlist - Import from VC Todo',

	'shortlist_title' 		=> 'Title',
	'shortlist_Internal' 	=> 'Internal',
	'shortlist_External'	=> 'External',


	'period_year' => 'year',
	'period_month' => 'month',
	'period_day' => 'day',
	'period_hour' => 'hour',
	'period_min' => 'minute',
	'period_sec' => 'second',
	'period_postfix' => 's',
	'period_ago' => 'ago',
	'period_now' => 'just now',

	'period_years' => 'years',
	'period_months' => 'months',
	'period_days' => 'days',
	'period_hours' => 'hours',
	'period_mins' => 'minutes',
	'period_secs' => 'seconds',




	'shortlist_list_item_single' => 'List Item',
	'shortlist_list_item_plural' => 'List Items',

	'shortlist_item_single'			=> 'Item',
	'shortlist_item_plural'			=> 'Items',

	'shortlist_user_list_single'	=> 'User List',
	'shortlist_user_list_plural'	=> 'User Lists',

	'shortlist_list_single'			=> 'List',
	'shortlist_list_plural'			=> 'Lists',

	'shortlist_internal'			=> 'internal',
	'shortlist_external'			=> 'external',

	'shortlist_user_or_guest'		=> 'User (or Guest)',
	'shortlist_last_active'			=> 'Last Activity',
	'shortlist_guest'				=> 'Guest',

	'shortlist_details_about_this_item'	=> 'Details about this Item',	
	'shortlist_view_entry'				=> 'View Entry',
	'shortlist_external_item_details'	=> 'External Item Details',
	'shortlist_unique_marker'			=> 'Unique Marker',
	'shortlist_item_appears_in'			=> 'This Item appears in',

	'shortlist_details_about_this_list'	=> 'Details about this List',
	'shortlist_owned_by'				=> 'Owned by',
	'shortlist_share_id'				=> 'Share Id',
	'shortlist_list_has'				=> 'This list has',
	'shortlist_added'					=> 'Added',
	'shortlist_items_have_been_cloned'	=> 'Items from this list have been cloned',
	'shortlist_times_single'			=> 'time',
	'shortlist_times_plural'			=> 'times',
	'shortlist_cloned_by'				=> 'Cloned By',
	'shortlist_when'					=> 'When',


	'count'							=> 'Count',
	'type'							=> 'Type',
	'item_count'					=> 'Item Count',

	'' => ''
);